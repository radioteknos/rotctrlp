/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@amsat.org
 */

#include <stdio.h>       
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>       
#include "daemon.h"

/* 
   DA FARE:

   1) Questo programma si deve comportare bene come deamon
   2) Documentazione
 */

/*
  Il giunto a Y lavora fra sei FIFO: inH, inL, out, in, out1, out2; tali fifo
  si trovano tutte nella stessa directory specificata dalla linea di comando.
  Se non viene specificata nessuna directory viene usata la $HOME/.Y/

  Il giunto a Y e` composto di due parti, quella di concentrazione e quella
  di divisione: la prima scrive su "out" quello che viene da "inH" e da "inL",
  mentre la seconda legge "in" e lo copia sia su "out1" che su "out2".

       InH -----\                              /----- out1
                 >----- out           in -----<
       InL -----/                              \----- out2

  Attraverso la fifo inH e` possibile terminare il giunto a Y inviando la
  stringa "QY\n".

  Il logfile del traffico puo` essere fatto con la redirezione dello stdout
  un file o su /dev/null una volta abilitata l'opzione -v (verbose).

*/
 
int main(int argc, char **argv){
int fdih, fdil, fdout, fdi, fdo1, fdo2, lastfd=0, n; 
int clo, mode, create=0, verbose=0;
char buffer[80], dirpath[80], gfn[80];
fd_set rfds;
char fifoname[6][5]={"inH\0", "inL\0", "out\0", "in\0", "out1\0", "out2\0"};

/* Se nessuna directory e` specificata nella linea di comando allora si usa 
   la $HOME/.Y e $HOME si ricava dall'environment */
 strcpy(dirpath, getenv("HOME"));
 if(dirpath==NULL) {
   printf("Unable to find HOME dir ?!\n"); return 1;
 }
 strcat(dirpath, "/.Y/");
 
 /* Scansione della riga di comando */
 if(argc>1) {
   while(1){
     clo=getopt(argc, argv, "d:chv");
     if(clo==-1) break;
     switch(clo){
     case 'd': if(sscanf(optarg, "%s", dirpath)==0 ) 
       { printf("Invalid path for fifos\n"); return 1; } break; 
     case 'c': create=1; break;
     case 'h':
       printf("\nY junction:\n -d <dirpath> where fifos are stored\n");
       printf(" -c create fifos if they don't exist\n");
       printf(" -v print on stderr whenever data arrival\n");
       printf(" -h this help\n");
       printf("\nString \"QY\\n\" in %s will terminate Y junction.\n\n", 
	      fifoname[0]);
       return(0);
     case 'v': verbose=1; break;
     }
   }
 }

 /* Procedura per far diventare un daemon, se possibile */
 if(verbose==0){
   if(daemon_start(1)==0) {
     fprintf(stderr, "rotord cannot beacame a daemon\n");
     return 1;
   }
 }
 else {
   printf("Using verbose option rotord cannot be a daemon.\n");
 }
 
 
 /* Se le fifo mancanti devono essere create...*/
 if(create==1){
   /* innanzitutto si guarda se c'e` la directory o se la si puo` creare... */
   mode=F_OK; 
   if(access(dirpath, mode)==-1) {
     if(mkdir(dirpath, 0600)==-1) {
       printf("Unable to create %s\n", dirpath); 
       return 1;
     }
   }
   /* e poi si creano, all'occorrenza, le fifo. */
   for(n=0; n<6; n++){
     mode=F_OK; strcpy(gfn, dirpath); strcat(gfn, fifoname[n]);
     if(access(gfn, mode)==-1){
       if(mkfifo(gfn, 0600)==-1){
	 printf("%s not found and unable to create it in %s\n", 
		fifoname[n], dirpath);
	 return 1;
       }
     }
   }
 }
   
   
   /* Apertura fifo della parte concentratrice */
   strcpy(gfn, dirpath); strcat(gfn, fifoname[0]);
   if((fdih=open(gfn, O_RDWR|O_NONBLOCK))==-1)
     {fprintf(stderr, "Unable to open input High priority fifo %s\n",
	      fifoname[0]); return 1;}
   strcpy(gfn, dirpath); strcat(gfn, fifoname[1]);
   if((fdil=open(gfn, O_RDWR|O_NONBLOCK))==-1)
     {fprintf(stderr, "Unable to open input Low priority fifo %s\n",
	      fifoname[1]); return 1;}
   strcpy(gfn, dirpath); strcat(gfn, fifoname[2]);
   if((fdout=open(gfn, O_RDWR|O_NONBLOCK))==-1)
     {fprintf(stderr, "Unable to open output fifo %s\n",
	      fifoname[2]); return 1;}
   
   
   /* Apertura fifo della parte divisrice */
   strcpy(gfn, dirpath); strcat(gfn, fifoname[3]);
   if((fdi=open(gfn, O_RDWR|O_NONBLOCK))==-1)
     {fprintf(stderr, "Unable to open input fifo %s\n",
	      fifoname[3]); return 1;}
   strcpy(gfn, dirpath); strcat(gfn, fifoname[4]);
   if((fdo1=open(gfn, O_RDWR|O_NONBLOCK))==-1)
     {fprintf(stderr, "Unable to open first output fifo %s\n",
	      fifoname[4]); return 1;}
   strcpy(gfn, dirpath); strcat(gfn, fifoname[5]);
   if((fdo2=open(gfn, O_RDWR|O_NONBLOCK))==-1)
     {fprintf(stderr, "Unable to open second output fifo %s\n",
	      fifoname[5]); return 1;}
   
   
   /* Impostazioni per la select() */
   if(fdih>lastfd)
     lastfd=fdih;
   if(fdil>lastfd)
     lastfd=fdil; 
   if(fdi>lastfd)
     lastfd=fdi; 
   FD_ZERO(&rfds); 
   FD_SET(fdih, &rfds); FD_SET(fdil, &rfds); FD_SET(fdi, &rfds);
   lastfd++;
   
   /* Ciclo principale */
   while(1){ 
     select(lastfd, &rfds, NULL, NULL, NULL);
     
     if(FD_ISSET(fdih, &rfds)) { 
       n=read(fdih, buffer, 79);
       if(strncmp(buffer, "QY\n", 3)==0) break;
       if(verbose==1) fprintf(stderr, "DATA from %s: %d bytes\n", 
			      fifoname[0], n);
       write(fdout, buffer, n);
       
       /* Senza non funziona ma come mai ci vuole?
	  La documentazione non lo dice */
       FD_ZERO(&rfds); 
       FD_SET(fdih, &rfds); FD_SET(fdil, &rfds); FD_SET(fdi, &rfds); 
       continue;
     }
     
     if(FD_ISSET(fdil, &rfds)) {
       n=read(fdil, buffer, 79);
       if(verbose==1) fprintf(stderr, "DATA from %s: %d bytes\n",
			      fifoname[1], n);
       write(fdout, buffer, n);
       
       /* Senza non funziona ma come mai ci vuole?
	  La documentazione non lo dice */
       FD_ZERO(&rfds); 
       FD_SET(fdih, &rfds); FD_SET(fdil, &rfds); FD_SET(fdi, &rfds); 
       continue;
     }
     
     if(FD_ISSET(fdi, &rfds)) {
       n=read(fdi, buffer, 79);
       if(verbose==1) fprintf(stderr, "DATA from %s: %d bytes\n",
			      fifoname[3], n);
       write(fdo1, buffer, n);
       write(fdo2, buffer, n);
       
       /* Senza non funziona ma come mai ci vuole?
	  La documentazione non lo dice */
       FD_ZERO(&rfds); 
       FD_SET(fdih, &rfds); FD_SET(fdil, &rfds); FD_SET(fdi, &rfds); 
       continue;
     }
     
   }
   
   fprintf(stderr,"Exiting...\n");
   close(fdo2); close(fdo1); close(fdi);
   close(fdout); close(fdil); close(fdih);
   return 0;
 }
