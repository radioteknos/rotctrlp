    azl=[]
    ell=[]
    try:
        val=cfg.get('constrain', 'minel').split('\n')
        for v in val:
            if(v!=''):
                if(len(v.split())==2):
                    azl.append(float(v.split()[0]))
                    ell.append(float(v.split()[1]))
        minelf=interp1d(azl, ell)    
    except (configparser.NoSectionError, configparser.NoOptionError):
        pass
    azl=[]
    ell=[]
    try:
        val=cfg.get('constrain', 'maxel').split('\n')
        for v in val:
            if(v!=''):
                if(len(v.split())==2):
                    azl.append(float(v.split()[0]))
                    ell.append(float(v.split()[1]))
        maxelf=interp1d(azl, ell)    
    except (configparser.NoSectionError, configparser.NoOptionError):
        pass

    #N on si puo` fare perche' non contano solo le costrizioni sui valori
    # minimi e massimi dell'EL in funzione di AZ perche' andrebbe calcolata
    # la traiettoria che venga fatta per passare da un punto all'altro:
    # insomma non c'e` un modo semplice per scansare i tiranti
    # il file naxrotd.conf era pensato come due tabelle di EL min e max
    # in funzione di AZ
