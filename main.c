/*
 *   rotctrlp - antenna rotator control panel
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <fcntl.h> 
#include "main.h"

WINDOW *statwin, *remcmdwin;

int ml=1, sfdcmd, verbose;
struct sockaddr_in cmdch, ansch;
int cmdch_port=1300, ansch_port=1301;
socklen_t cmdch_addr_size;

int main(int argc, char **argv){
  int clo, n, rv, i;
  char buff[BUFFLEN], *cmd, *valbuff, timestampbuff[16],
    naxrot2d_addr[BUFFLEN]="127.0.0.1";
  float v;
  float az=0., el=0., setaz=0., setel=0, lastaz=0., lastel=0., laststep=5.;
  int fdi, lastfd=0, sfdans;
  fd_set rfds;
  struct timeval timeout;
  WINDOW *inpwin;

  char sockbuff[BUFFLEN], *pb;
  
  /* catch ctrl+C */
  signal(SIGINT, quit);

  /* variable init */
  verbose=0;

  
  /* cmd line scan */
  if(argc>1) {
    while(1){
      clo=getopt(argc, argv, "hva:b");
      if(clo==-1) break;
      switch(clo){
      case 'h':
	use();
	return(0);
      case 'v':
	verbose=1;
	break;
      case 'a':
	if(strlen(optarg)>BUFFLEN-1){
	  printf("ip address string too long!\n");
	  return(1);
	}
	if(sscanf(optarg, "%s", naxrot2d_addr)==0 ) { 
	  printf("invalid ip address\n");
	  return(1);
	}
	break;
	
      }
    }
  }

  /* fifo open */
  n=fifo_open_create(&fdi, 0, "");
  if(n==-1)
    return(1);  /* user exit */
  else if(n==1)
    return(0);  /* error */

  /* socket init */
  sfdcmd=socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0); 
  cmdch.sin_family=AF_INET;
  cmdch.sin_port=htons(cmdch_port);
  cmdch.sin_addr.s_addr=inet_addr(naxrot2d_addr);
  memset(cmdch.sin_zero, '\0', sizeof(cmdch.sin_zero));
  cmdch_addr_size=sizeof(cmdch);

  memset(&ansch, 0, sizeof(ansch));
  ansch.sin_family=AF_INET; 
  ansch.sin_addr.s_addr=htonl(INADDR_ANY);
  ansch.sin_port=htons(ansch_port);

  sfdans=socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, IPPROTO_UDP);
  if(sfdans==-1){
   fprintf(stderr, "unable to create socket on port %d\n", ansch_port);
   return(1);
  }

  rv=bind(sfdans, (struct sockaddr *)&ansch, sizeof(ansch));
  if(rv==-1){
    fprintf(stderr, "unable to bind port %d to addr %s\n",
	    ansch_port, inet_ntoa(ansch.sin_addr));
    return(1);
  }

  
  /* ncurses init */
  initscr(); cbreak(); noecho(); timeout(0); start_color(); curs_set(0);

  init_pair(1, COLOR_GREEN, COLOR_BLACK);
  init_pair(2, COLOR_YELLOW, COLOR_BLACK);
  init_pair(3, COLOR_WHITE, COLOR_BLACK);
  init_pair(4, COLOR_BLACK, COLOR_CYAN);
  init_pair(5, COLOR_RED, COLOR_BLACK);
  init_pair(6, COLOR_CYAN, COLOR_BLACK);
  init_pair(7, COLOR_BLUE, COLOR_CYAN);
  init_pair(8, COLOR_BLACK, COLOR_YELLOW);

  /* fixed part of tui */
  attron(COLOR_PAIR(3));
  attroff(A_BOLD);
  box(stdscr, ACS_VLINE, ACS_HLINE);
  attron(COLOR_PAIR(5));
  attron(A_BOLD);
  mvprintw(0,10, " ROTCTRLP ");
  attron(COLOR_PAIR(3));
  attroff(A_BOLD);
  mvprintw(22,2, "%s", __VERSION);
  refresh();

  /* remote cmd window */
  if((remcmdwin=newwin(6, 32, 7, 2))==NULL) {
    fprintf(stderr, "Unable to create control panel status window\n");
    return 1;
  }
  wbkgd(remcmdwin, COLOR_PAIR(8));
  scrollok(remcmdwin, 1);
  wprintw(remcmdwin, " --- ready ---\n");
  wrefresh(remcmdwin);
  
  /* status/error window */
  if((statwin=newwin(8, 32, 14, 2))==NULL) {
    fprintf(stderr, "Unable to create control panel status window\n");
    return 1;
  }
  wbkgd(statwin, COLOR_PAIR(4));
  scrollok(statwin, 1);
  utctimestr(timestampbuff);
  wprintw(statwin, " --- start %s ---\n", timestampbuff);
  wrefresh(statwin);

  /* select() setup */
  if(lastfd<fdi)
    lastfd=fdi;
  if(lastfd<sfdans)
    lastfd=sfdans;
  FD_ZERO(&rfds);
  FD_SET(fdi, &rfds);
  FD_SET(sfdans, &rfds);
  lastfd++;
  timeout.tv_sec=0;
  timeout.tv_usec=200000;
  
  while(ml){

    attron(COLOR_PAIR(2)); attron(A_BOLD);
    mvprintw(1,6, "AZ %5.1f", az);
    mvprintw(1, 19, "EL %5.1f", el);
    attroff(A_BOLD);
    
    attron(COLOR_PAIR(1));
    mvprintw(3,2, "lastAZ %5.1f", lastaz);
    mvprintw(3,15, "lastEL %5.1f", lastel);
    mvprintw(5,3, "setAZ %5.1f", setaz);
    mvprintw(5,16, "setEL %5.1f", setel);
    
    refresh();

    rv=select(lastfd, &rfds, NULL, NULL, &timeout);
    switch(rv){

    case -1:                                                     /* error */
      /* TODO: handle errors? */
      break;
      
    case 0:                               /* timeout: check ncurses input */
      switch(rv=getch()){

      case -1:
	break;
	
      case 'a':                                          /* set AZ */
	if((inpwin=newwin(1, 6, 5, 3))==NULL)
	  error_beep();
	else{	  
	  wattron(inpwin, COLOR_PAIR(4) | A_BOLD);
	  echo();
	  mvwprintw(inpwin, 0, 0, "_____");
	  if(mvwscanw(inpwin, 0, 0, "%f", &v)==0){
	    error_beep();
	    delwin(inpwin);
	    noecho();
	    break;
	  }
	  if(v<0. || v>360.0)
	    error_beep();
	  else{
	    setaz=v;
	    send_drv_cmd_set(cmdSETAZ, setaz);
	  }
	}
	noecho();	
	break;

      case 'e':                                          /* set EL */
	if((inpwin=newwin(1, 6, 5, 15))==NULL)
	  error_beep();
	else{	  
	  wattron(inpwin, COLOR_PAIR(4) | A_BOLD);
	  echo();
	  mvwprintw(inpwin, 0, 0, "_____");
	  if(mvwscanw(inpwin, 0, 0, "%f", &v)==0){
	    error_beep();
	    delwin(inpwin);
	    noecho();
	    break;
	  }
	  if(v<0. || v>90.0)
	    error_beep();
	  else{
	    setel=v;
	    send_drv_cmd_set(cmdSETEL, setel);
	  }
	}
	noecho();	
	break;

      case 's':                                          /* set STEP */
	if((inpwin=newwin(1, 11, 2, 3))==NULL)
	  error_beep();
	else{
	  wattron(inpwin, COLOR_PAIR(4) | A_BOLD);
	  echo();
	  mvwprintw(inpwin, 0, 0, "STEP _____");
	  if(mvwscanw(inpwin, 0, 5, "%f", &v)==0){
	    error_beep();
	    delwin(inpwin);
	    noecho();
	    break;
	  }
	  wattron(inpwin, COLOR_PAIR(1));
	  mvwprintw(inpwin, 0, 0, "          ");
	  wrefresh(inpwin);
	  if(v<0. || v>30.0)
	    error_beep();
	  else{
	    laststep=v;
	    /* TODO: what to do? It's not usefull for now and will
	     require a complete system of ack and resend... maybe could 
	     be good for naxrot3d */
	    /* send_drv_cmd_set(cmdSETSTEP, laststep); */
	  }
	}
	delwin(inpwin);
	noecho();	
	break;

	
      case 0x0b: /* ctrl + k  =  quit (unload) naxrot2d and exit */
	ml=0;
	send_drv_cmd_set(cmdQUIT, 0);
	break;

      case 0x15: /* ctrl + u  =  quit (unload) naxrot2d */
	send_drv_cmd_set(cmdQUIT, 0);
	break;

	
      case 0x1b:
	switch(rv=getch()){
	case 0x5b:
	  switch(getch()){
	  case 0x63:  /* shift + curs rigth -> AZ++ */
	    setaz+=laststep;
	    if(setaz>360.){
	      setaz=360.;
	    }
	    send_drv_cmd_set(cmdSETAZ, setaz);	    
	    break;
	  case 0x64:  /* shift + curs left -> AZ++ */
	    setaz-=laststep;
	    if(setaz<0.){
	      setaz=0.;
	    }
	    send_drv_cmd_set(cmdSETAZ, setaz);	    
	    break;
	  case 0x61:  /* shift + curs up -> EL++ */
	    setel+=laststep;
	    if(setel>90.){
	      setel=90.;
	    }
	    send_drv_cmd_set(cmdSETEL, setel);	    
	    break;
	  case 0x62:  /* shift + curs down -> EL++ */
	    setel-=laststep;
	    if(setel<0.){
	      setel=0.;
	    }
	    send_drv_cmd_set(cmdSETEL, setel);	    
	    break;
	  default:
	    error_beep();
	    
#ifdef DEVEL /* activate this by issueing $ make CFLAGS="-DDEVEL" */
	    wprintw(statwin, "\nkbd 0x1b 0x5b 0x%02x", rv);
	    wrefresh(statwin);
#endif
	  }
	  break;
	default:
	  error_beep();
#ifdef DEVEL
	  wprintw(statwin, "\nkbd 0x1b 0x%02x", rv);
	  wrefresh(statwin);
#endif
	}
	break;
	
      default:
	error_beep();
#ifdef DEVEL
	wprintw(statwin, "\nkbd 0x%02x", rv);
	wrefresh(statwin);
#endif	
      } /* switch(getch()) */

      timeout.tv_sec=0;
      timeout.tv_usec=200000;
      
      FD_ZERO(&rfds);
      FD_SET(fdi, &rfds);
      FD_SET(sfdans, &rfds);
      break;
      
    default:                                                /* fd has data */
      if(FD_ISSET(fdi, &rfds)){                              /* fifo (trk) */
	utctimestr(timestampbuff);
	wprintw(remcmdwin, "%s - ", timestampbuff);
	
	n=read(fdi, buff, BUFFLEN-1);      
	buff[n]=0;
	wprintw(remcmdwin, "%s", buff);
	wrefresh(remcmdwin);
	buff[strcspn(buff, "\r\n")]='\0';

	cmd=strtok(buff, " ");
	for(i=0; i<(int)strlen(cmd); i++)
	  cmd[i]=toupper(cmd[i]);

	if(strcmp(buff, cmdTRKAZ)==0){
	  valbuff=strtok(NULL, " ");

	  if(sscanf(valbuff, "%f", &v)>0){
	    if(v>0. && v<=360.){
	      setaz=v;
	      send_drv_cmd_set(cmd, setaz);
	    }
	    else
	      wprintw(statwin, "\nvalue out of range: %5.1f", v);
	  }
	  else
	    wprintw(statwin, "\nwrong arg: %s", valbuff);
	  wrefresh(statwin);
	}
	
	else if(strcmp(buff, cmdTRKEL)==0){
	  valbuff=strtok(NULL, " ");

	  if(sscanf(valbuff, "%f", &v)>0){
	    if(v>0. && v<=90.){
	      setel=v;
	      send_drv_cmd_set(cmd, setel);
	    }
	    else
	      wprintw(statwin, "\nvalue out of range: %5.1f", v);
	  }
	  else
	    wprintw(statwin, "\nwrong arg: %s", valbuff);
	  wrefresh(statwin);
	}
	
	else{
	  utctimestr(timestampbuff);
	  wprintw(statwin, "\n%s - unk cmd: %s", timestampbuff, cmd);
	  wrefresh(statwin);
	}
	
		
	FD_ZERO(&rfds);
	FD_SET(fdi, &rfds);
	FD_SET(sfdcmd, &rfds);
      }
      else if(FD_ISSET(sfdans, &rfds)){                 /* naxrot2d socket */
	
	n=recv(sfdans, sockbuff, BUFFLEN-1, 0);
	sockbuff[n]='\0';

	if(verbose>0){
	  wprintw(statwin, "\n[rcvd]: %s (%d)", sockbuff, strlen(sockbuff));
	  wrefresh(statwin);
	}

	pb=(char*)sockbuff; skipspace(&pb);
	if(strncasecmp(pb, cmdLASTAZ, 6)==0){
	  skipnotspace(&pb);
	  /* TODO: add error chack to sscanf() to preserve variables */
	  sscanf(pb, "%f", &lastaz);
	}
	if(strncasecmp(pb, cmdLASTEL, 6)==0){
	  skipnotspace(&pb);
	  sscanf(pb, "%f", &lastel);
	}
	if(strncasecmp(pb, cmdBACKAZ, 2)==0){
	  skipnotspace(&pb);
	  sscanf(pb, "%f", &az);
	}
	if(strncasecmp(pb, cmdBACKEL, 2)==0){
	  skipnotspace(&pb);
	  sscanf(pb, "%f", &el);
	}
	/* TODO: decode others answers */
	
	FD_ZERO(&rfds);
	FD_SET(fdi, &rfds);
	FD_SET(sfdans, &rfds);
      }
    } /* # select switch end # */
  } /* while(ml) */
  
  clear(); refresh(); endwin();
  close(sfdans);
  return 0;
}


void utctimestr(char *buff){
  time_t t;
  struct tm *tmp;
  const char *fmt="%H:%M:%SZ";

  t=time(NULL);
  tmp=gmtime(&t);

  strftime(buff, 10, fmt, tmp);
}


int fifo_open_create(int *fdi, int create, char *path){
  char homedir[40], cfgdir[40], gf[80], buff[80];

 if(strlen(path)==0){
   strcpy(homedir, getenv("HOME"));
   if(homedir==NULL) {
     fprintf(stderr, "Unable to find HOME dir ?!\n");
     return(-1);
   }
   strcat(homedir, "/");
   strcpy(cfgdir, homedir);
   strcat(cfgdir, TRKDIR);
 }
 else {
   strcpy(cfgdir, path);
   if(cfgdir[strlen(cfgdir)-1]!='/') strcat(cfgdir, "/");
 }

 
 if(access(cfgdir, F_OK)==-1) {
   printf("Unable to find fifo dir: %s\ncreate? [y]/n\n", cfgdir);
   fgets(buff, sizeof(buff), stdin);
   buff[strcspn(buff, "\r\n")]='\0';
   if(strcasecmp(buff, "y")==0){
     if(mkdir(cfgdir, 0755)==-1){
       fprintf(stderr, "Unable to create fifo dir: %s\n", cfgdir);
       return(-1);
     }
   }
   else
     return(1);
 }   
 
 strcpy(gf, cfgdir);
 strcat(gf, FIFOINDEFNAME);
 if(access(gf, F_OK)==-1){
   if(create==0) {
     printf("Unable to find fifo: %s\ncreate? [y]/n\n", gf);
     fgets(buff, sizeof(buff), stdin);
     buff[strcspn(buff, "\r\n")]='\0';
     if(strcasecmp(buff, "y")==0){
       if(mkfifo(gf, 0600)==-1){
       fprintf(stderr, "Unable to create input fifo: %s\n", cfgdir);
       return(-1);
     }
   }
   else
     return(1);
   }
   else {
     if(mkfifo(gf, 0600)==-1) {
       fprintf(stderr, "Unable to create input fifo: %s\n", gf);
       return(-1);
     }
   }
 }

 if((*fdi=open(gf, O_RDWR | O_NDELAY))==-1) {
   fprintf(stderr, "Unable to open input fifo: %s\n", gf);
   return(-1);
 } 
 
 return 0;
}

void send_drv_cmd_set(char *cmd, float val){
  #define MSGBUFFLEN 31
  char msgbuff[MSGBUFFLEN+1];
  
  if(verbose>0){
    wprintw(statwin, "\n[send]: %s %5.1f", cmd, val);
    wrefresh(statwin);
  }

  snprintf(msgbuff, MSGBUFFLEN, "%s %5.1f", cmd, val);
  sendto(sfdcmd, msgbuff, strlen(msgbuff), 0,
  	 (struct sockaddr *)&cmdch, cmdch_addr_size);
  
}

void error_beep(void){
  move(0, 0);
  printf("\a");
  fflush(stdout);
}

void quit(int signo){
  if(signo==SIGINT)
    ml=0;
}

unsigned int skipspace(char **b){
  while(((*b)[0]==' ' || (*b)[0]=='\t') && (*b)[0]!='\0') 
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

unsigned int skipnotspace(char **b){
  while(*b[0]!=' ' && *b[0]!='\t' && *b[0]!='\0')
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

void use(void){
  
  printf("\nAntenna Rotator Control Pannel (rotctrlp) usage:\n\n");
  printf("  rotctrlp [-h] [-v] [-a <ip_addr>]\n");
  printf("  -h (this) help\n");
  printf("  -v verbose, print some debugging msg on statwin\n");
  printf("  -a <ip_addr> specify ip address where naxrot2d is running (default 127.0.0.1)\n");
  printf("\n    keystrokes:\n");
  printf("    'a' - set AZ\n");
  printf("    'e' - set EL\n");
  printf("    's' - set STEP\n");
  printf("    'shift + cursor' - move one step AZ or EL\n");
  printf("    'ctrl + u' - terminate naxrot2d\n");
  printf("    'ctrl + c' - quit rotctrlp\n");
  printf("    'ctrl + k' - terminate naxrot2d and quit rotctrlp\n");
  printf("\n");
}
