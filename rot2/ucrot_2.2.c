/* UCROT:
   Controllo rotore 2  ASCII
   
   v1.2 --- lavoro in corso

*/

#pragma RS232_RXPORT PORTB
#pragma RS232_TXPORT PORTB
#pragma RS232_RXPIN 0
#pragma RS232_TXPIN 1
#pragma RS232_BAUD 9600
#pragma TRUE_RS232 1
#pragma CLOCK_FREQ 4000000


#define XON  17
#define XOFF 19

/* Variabili globali */
char i, j, stackw, stackst;
unsigned char rxstat;
unsigned char stat, hist, incr;
unsigned int val, temp, az, el, azenc, elenc, enc, acc;



/* Definizioni per la variabile stat */
#define RETR     4         /* Occorre fare una ritrasmissione */
#define HASVAL   5         /* Il dato seriale ricevuto ha il campo numerico */
#define MDIR     6         /* Direzione del motore (uso interno alle motaz
			      e motel) */
#define ENCAZEL  7         /* Lettura encoder AZ (se 0) oppure EL (se 1)
			      (non `e proprio uno stato, ma gli ultimi due bit
			      della variabile stat non vengono trasmessi e 
			      quindi si possono impiegare per altri scopi */

/* Definizioni per la variabile rxstat */
#define DATARDY  7
#define DF2      6       /* 0 0 -- Int val       */
#define DF1      5       /* 0 1 -- Decimal point */
                         /* 1 0 -- DP & 1 decimal figure */
                         /* 1 1 -- DP & >1 decimal figure (err) */
#define CMDVAL4  4       /* Queste definizioni non verranno mai usate, */
#define CMDVAL3  3       /* sono state messe per ricordarsi come e`    */
#define CMDVAL2  2       /* organizzata la rxstat                      */
#define CMDVAL1  1
#define CMDVAL0  0

/* Piedini porta A */
#define CWAZ     2
#define CCWAZ    3
#define CWEL     0
#define CCWEL    1

/* Piedini porta B */
#define SERIN    0
#define SEROUT   1
#define CS       2
#define CLK      3
#define DIN      4
#define DOUT     5

/* Funzioni */
void bin2putchar(void);
void readenc(void);
void motaz(void);
void motel(void);
void err2(void);
void err3(void);
void err4(void);
void mul10acc(void);

/* EEPROM */
#define AZo 0x01
#define ELo 0x03
#define HiI 0x05
#define InI 0x07


void main(void){

_reset:

/* Impostazione variabili globali */
rxstat=0; stat=0;

/* init */
asm bsf STATUS, RP0
set_tris_a(0x10);
set_tris_b(0xe1);
set_option(0x01);
asm bcf EECON1, WRERR;
asm bcf STATUS, RP0;

/* Prima che la porta seriale in uscita venga usata la si mette al livello
   di riposo */
output_high_port_b(SEROUT);


/* Motori fermi */
output_low_port_a(CWAZ); output_low_port_a(CCWAZ);
output_low_port_a(CWEL); output_low_port_a(CCWEL);

enable_interrupt(GIE);
enable_interrupt(INTE);
disable_interrupt(RBIE);
disable_interrupt(T0IE);
disable_interrupt(EEIE);

hist=10; /* Nel seguito sara` letta dalla EEPROM */
incr=50; /* Nel seguito sara` letto dalla EEPROM */
val=0; acc=0;

/* Trasmissione messaggio iniziale */
putchar('\n'); putchar('\r');
putchar('N'); putchar('A'); putchar('X'); 
putchar('R'); putchar('O'); putchar('T');
putchar('2'); putchar('\n'); putchar('\r');

 clear_bit(stat,ENCAZEL); /* Impostazione AZ e EL ai valori su cui si trova */
 readenc();
 asm{
   movf _enc, W
   movwf _az
   movf _enc+1, W
   movwf _az+1
     }
 set_bit(stat,ENCAZEL);    
 readenc();
 asm{
   movf _enc, W
   movwf _el
   movf _enc+1, W
   movwf _el+1
     }

 /* Anello principale */
 while(1){

 main_loop:
   
   /* Controllo che i valori siano nel campo ammesso */
   /* Non si riesce a farlo per ogni impostazione, va fatto generico qui */

 main_cazi:
   asm { 
     movf _az+1, 0;
     sublw 0xfe;
     btfsc STATUS, C;
     goto _main_cazs;
     clrf _az+1;
     clrf _az;
   }

 main_cazs:
   asm {
     movf _az+1, 0;
     sublw 0x0d;
     btfsc STATUS, C;
     goto _main_celi;
     movf _az, 0;
     sublw 0x0f;
     btfsc STATUS, C;
     goto _main_celi;
     movlw 0x0e;
     movwf _az+1;
     movlw 0x10;
     movwf _az
   }

 main_celi:
   asm { 
     movf _el+1, 0;
     sublw 0xfe;
     btfsc STATUS, C;
     goto _main_cels;
     clrf _el+1;
     clrf _el;
   }
   
 main_cels:
   asm {
     movf _el+1, W;
     sublw 0x06;
     btfsc STATUS, C;
     goto _main_mot;
     movf _el, W;
     sublw 0x07;
     btfsc STATUS, C;
     goto _main_mot;
     movlw 0x07;
     movwf _el+1;
     movlw 0x08
     movwf _el;
   }
   
 main_mot:

   /* Lettura encoder e movimento motori */
   clear_bit(stat,ENCAZEL);    
   readenc();
   asm{
     movf _enc, W
     movwf _azenc
     movf _enc+1, W
     movwf _azenc+1
   }
   motaz();    

   set_bit(stat,ENCAZEL);    
   readenc();
   asm{
     movf _enc, W
     movwf _elenc
     movf _enc+1, W
     movwf _elenc+1
   }
   motel();

   /* Ritrasmissioni */
   if(stat&0x10){
     j=rxstat&0x1f; j=j+0x60;
     switch(j){
     case 'a': putchar('A'); clear_bit(stat,ENCAZEL); readenc(); /* val=enc */
       asm {
       movf _enc+1, W;
       movwf _val+1;
       movf _enc, W;
       movwf _val;
       }
       bin2putchar(); break;
     case 'e': putchar('E'); set_bit(stat, ENCAZEL); readenc(); /* val=enc */
       asm {
       movf _enc+1, W;
       movwf _val+1;
       movf _enc, W;
       movwf _val;
       }
       bin2putchar(); break;
     case 'h': putchar('H'); /* val=hist&0x00ff */
       asm {
	 clrf _val+1;
	 movf _hist, W;
	 movwf _val;
       }
       bin2putchar(); break;
     case 'i': putchar('I'); /* val=az */
       asm{ 
       movf _az+1, W;
       movwf _val+1;
       movf _az, W;
       movwf _val;
       } 
       bin2putchar(); break; 
     case 'j': putchar('J'); /* val=el */
       asm {
       movf _el+1, W;
       movwf _val+1;
       movf _el, W;
       movwf _val;
       }
       bin2putchar(); break;
     case 'p': putchar('P'); /* val=incr0x00ff */
       asm {
	 clrf _val+1;
	 movf _incr, W;
	 movwf _val;
       }
       bin2putchar(); break;

     case 'f': putchar('F');
       asm{
	 movlw 0x01;   /* AZo - EEPROM */
	 movwf EEADR;
	 bsf STATUS, RP0
	 bsf EECON1, 0;
	 bcf STATUS, RP0
	 clrf _val+1;
	 movf EEDATA, W;
	 movwf _val;
	 } bin2putchar(); break;

     case 'g': putchar('G');
       asm{
	 movlw 0x03;   /* ELo - EEPROM */
	 movwf EEADR;
	 bsf STATUS, RP0
	 bsf EECON1, 0;
	 bcf STATUS, RP0
	 clrf _val+1;
	 movf EEDATA, W;
	 movwf _val;
	 } bin2putchar(); break;

     case 's': asm {
       movf PORTA, W;
       andlw 0x1f;
       movwf _val;
       btfsc PORTB, 6; /* MAA */
       bsf _val, 5;
       btfsc PORTB, 7; /* MAE */
       bsf _val, 6;

       ; /* Il bit 7 e` disponibile per una segnalazione (temperatura?)*/
       ; /* i primi 5 bit di _val+1 sono diponibili per le segnalazioni */ 

     } bin2putchar(); break;
	 
     case 'z': goto _reset;
     }
     clear_bit(stat, RETR); rxstat=0; /* putchar(XON);*/
   }

   /* Aggiornamento variabili da ricezione seriale */
   if(rxstat&0x80){

     /* C'e' il dato ma anche un errore nella stringa del valore numerico */
     if(rxstat&0x60==0x60) {
     err2(); rxstat=0; /* putchar(XON); */ goto main_loop;
     }

     i=rxstat&0x1f; i=i+0x60;

     /* Il comando ha un valore numerico */
     if(stat&0x20){

       if(rxstat&0x40==0) mul10acc();

       switch(i){

       case 'a': /* az=acc */
	 asm {
	   movf _acc+1, 0;
	   movwf _az+1;
	   movf _acc, 0;
	   movwf _az;
	   }
	 break;

       case 'e': /* el=acc */
	 asm {
	   movf _acc+1, 0;
	   movwf _el+1;
	   movf _acc, 0;
	   movwf _el;
	 }
	 break;

       case 'h': /* hist=acc&0x00ff */ 
	 asm {
	   movf _acc, 0;
	   movwf _hist;
	   bsf _stat, 4; /* RETR */
	 }
	 break;

       case 'p': /* incr=acc&0x00ff */
	 asm {
	   movf _acc, 0;
	   movwf _incr;
	   bsf _stat, 4; /* RETR */
	 }
	 break;

       case 'f':
	 asm {
	   bcf INTCON, GIE;
	   bsf STATUS, RP0
	   bsf EECON1, WREN;
	   bcf STATUS, RP0
	   movlw 0x01; /* AZo - EEPROM */
	   movwf EEADR;
	   movf _acc, W;
	   addwf _az, F;
	   btfsc STATUS, C;
	   incf _az+1, F;
	   movwf EEDATA;
	   bsf STATUS, RP0
	   movlw 0x55;
	   movwf EECON2;
	   movlw 0xaa;
	   movwf EECON2;
	   bsf EECON1, WR;
	 }
main_wwee1:
	 asm {
	   btfsc EECON1, WR;
	   goto _main_wwee1;
	   bcf EECON1, WREN;
	   bcf STATUS, RP0;
	   bsf INTCON, GIE;
	 }
	 break;

       case 'g':
	 asm {
	   bcf INTCON, GIE;
	   bsf STATUS, RP0
	   bsf EECON1, WREN;
	   bcf STATUS, RP0
	   movlw 0x03; /* ELo - EEPROM */
	   movwf EEADR;
	   movf _acc, W;
	   addwf _el, F;
	   btfsc STATUS, C;
	   incf _el+1, F;
	   movwf EEDATA;
	   bsf STATUS, RP0
	   movlw 0x55;
	   movwf EECON2;
	   movlw 0xaa;
	   movwf EECON2;
	   bsf EECON1, WR;
	 }
main_wwee2:
	 asm {
	   btfsc EECON1, WR;
	   goto _main_wwee2;
	   bcf EECON1, WREN;
	   bcf STATUS, RP0
	   bsf INTCON, GIE;
	 }
	 break;

       }
       clear_bit(stat, HASVAL); rxstat=0; acc=0;
     }

     /* Il comando non ha un valore numerico */
     else{
       switch(i){
       case 'l': az=az-incr; /* putchar(XON); */ rxstat=0; break;
       case 'r': az=az+incr; /* putchar(XON); */ rxstat=0; break;
       case 'u': el=el+incr; /* putchar(XON); */ rxstat=0; break;
       case 'd': el=el-incr; /* putchar(XON); */ rxstat=0; break;
       case 'a':
       case 'e':
       case 'h':
       case 'i':
       case 'j':
       case 'p':
       case 's':
       case 'f': 
       case 'g': 
       case 'z': set_bit(stat, RETR); break;
       case 0x60: rxstat=0; stat=0;

       default: err4();
	 rxstat=0; stat=0;
       }
     }
       
   }

   /* fine dell'anello principale */
 }

}

void err2(void){
  putchar('N'); putchar('2'); putchar('\n');  putchar('\r');
}

void err3(void){
  putchar('N'); putchar('3'); putchar('\n'); putchar('\r');
}

void err4(void){
  putchar('N'); putchar('4'); putchar('\n'); putchar('\r');
}


/*
  Questo interrupt casca solo quando arriva un dato seriale.
*/
void interrupt(void){
char rxb;

  disable_interrupt(INTE);
  asm{
    movwf _stackw
    movf STATUS, 0
    movwf _stackst
  }

  rxb=getchar();

  if(rxb==' ') goto reti;
  if(rxb=='\t') goto reti;
 
  if(rxb=='\n') {
    set_bit(rxstat, 7); /* putchar(XOFF); */ goto reti;
  } 
  if(rxb=='\r') {
    set_bit(rxstat, 7); /* putchar(XOFF); */  goto reti;
  }

  if(rxb>='a') { rxb=rxb-0x60; rxb=rxb&0x1f; rxstat=rxb; goto reti; }
  if(rxb=='.') { set_bit(rxstat, 5); goto reti; }
  if(rxb>='0') {
    if(rxstat&0x1f==0) { rxstat=0; stat=0; acc=0; err2(); }

    set_bit(stat, HASVAL);
    if(rxstat&0x60==0x40) rxstat=rxstat+0x20;    
    if(rxstat&0x60==0x20) rxstat=rxstat+0x20;    

    /* moltiplicazione per 10 di acc (variabile globale) */
    /* se si fosse messa dopo l'aggiornamento dell'accumulatore sarebbe
       venuta una moltiplicazione in piu` */
    mul10acc();    

    rxb=rxb-'0'; acc=acc+rxb;
    
    goto reti;
  }
  err2();

reti:
  clear_bit(INTCON, INTF);
  asm{
    movf _stackst,0
    movwf STATUS
    swapf _stackw,1
    swapf _stackw,0
  }
  enable_interrupt(INTE);
}

void mul10acc(void){ 
  /*  acc=acc<<1; temp=acc; acc=acc<<2; acc=acc+temp; */
  asm {
    ; /* acc=acc<<1 */
    bcf STATUS, C;
    rlf _acc, F;
    rlf _acc+1, F;

    ; /* temp=acc; */
    movf _acc, W;
    movwf _temp;
    movf _acc+1, W;
    movwf _temp+1;

    ; /* acc=acc<<2; */
    bcf STATUS, C;
    rlf _acc, F;
    rlf _acc+1, F;
    rlf _acc, F;
    rlf _acc+1, F;

    ; /* acc=acc+temp */
    movf _temp, W;
    addwf _acc, F;
    btfsc STATUS, C;
    incf _temp+1, F;
    movf _temp+1, W;
    addwf _acc+1, F;
  }
  
}


/*
  Conversione dei valori AZ, EL, Hist in stringa ASCII, tenendo conto che
  internamente sono memorizzati come interi di valore 10 volte piu` grande.
  Penso che anche questa routine possa essere migliorata di molto, magari 
  scritta in assembler.
*/
void bin2putchar(void)
     /*
       In effetti il parametro di ingresso e' la variabile globale val che 
       essendo a 16 bit non puo` essere passata a una funzione
     */
{   
  putchar(' ');  
  i=0x30; while(val>=1000) {val=val-1000; i++;} putchar(i);
  i=0x30; while(val>=100) {val=val-100; i++;} putchar(i);
  i=0x30; while(val>=10) {val=val-10; i++;} putchar(i); 
  putchar('.'); putchar(val+0x30); putchar('\r'); putchar('\n');

}

void readenc(void)
{
output_low_port_b(CLK);
output_low_port_b(CS);
                                /* Start */
output_high_port_b(DIN);
nop();
output_high_port_b(CLK);
nop();
output_low_port_b(CLK);
nop();
                                /* SGL/DIFF=SGL */
output_high_port_b(DIN);
output_high_port_b(CLK);
nop();
output_low_port_b(CLK);
nop();
                                /* Scelta canale */
 if(stat&0x80)  output_high_port_b(DIN); /* 0x80 e` la maschera del ENCAZEL */
else output_low_port_b(DIN);
output_high_port_b(CLK);
nop();
output_low_port_b(CLK);
nop();
                                /* MSB first */
output_high_port_b(DIN);
output_high_port_b(CLK);
nop();
output_low_port_b(CLK);
nop();
                                /* Qui c'era il controllo dell'errore 
				   sul protocollo dell'ADC ma la segnalazione
				   non e` mai stata fatta, quindi e` inutile
				   anche il controllo... */
enc=0; 
for(j=0; j<12; j++)
  {
    output_high_port_b(CLK);
    nop();
    output_low_port_b(CLK);
    nop();
      asm{
      bcf STATUS, C
      btfsc PORTB, 5             ; DOUT
      bsf STATUS, C
      rlf _enc, F
      rlf _enc+1, F
    }
  }
output_high_port_b(CLK);
output_high_port_b(CS);

/* Sottrazione offset */
 asm{
   ; /* Lettura EEPROM */
   movlw 0x01; /* AZo */
   btfsc _stat, 7;
   movlw 0x03; /* Elo */
   movwf EEADR;
   bsf STATUS, RP0;
   bsf EECON1, 0;
   bcf STATUS, RP0;
   movf EEDATA, W;

   ; /* Sottrazione enc-EEPROM */
   subwf _enc, F;
   btfss STATUS, C;
   decf _enc+1, F;

   ; /* Controllo enc>0 */
 }
if(enc>0xfe00) enc=0;

readenc_end:
}


void motaz(void){

  asm{
	movf	_azenc+1,0
	subwf	_az+1,0
	btfss	STATUS,Z
	goto	_ma1
	movf	_azenc,0
	subwf	_az,0
	btfss	STATUS,Z
	goto	_ma1
	  }
 maoff:
  asm{
	bcf	PORTA,2
	bcf	PORTA,3
	return
	  }
 ma1:
  asm{
	btfsc	STATUS,C
	goto	_ma2
	bcf	_stat,6
	movf	_az+1,0
	movwf	_elenc+1
	movf	_az,0
	movwf	_elenc
	goto	_mah
	  }
 ma2:
  asm{
	bsf	_stat,6
	movf	_azenc+1,0
	movwf	_elenc+1
	movf	_az+1,0
	movwf	_azenc+1
	movf	_azenc,0
	movwf	_elenc
	movf	_az,0
	movwf	_azenc
	  }
 mah:
  asm{
	comf	_elenc,1
	incf	_elenc,1
	btfsc	STATUS,Z
	decf	_elenc+1,1
	comf	_elenc+1,1
	movf	_elenc,0
	addwf	_azenc,1
	btfsc	STATUS,C
	incf	_azenc+1,1
	movf	_elenc+1,0
	addwf	_azenc+1,1
	  }
 mahist:
  asm{
	movf	_azenc+1,1
	btfss	STATUS,Z
	goto	_maon
	movf	_hist,0
	subwf	_azenc,0
	btfss	STATUS,C
	goto	_maoff
	  }
 maon:
  asm{
	btfsc	_stat,6
	goto	_macw
	bcf	PORTA,2
	bsf	PORTA,3
	return
	  }
 macw:
  asm{
	bsf	PORTA,2
	bcf	PORTA,3
	return
	  }
}

void motel(void){
  asm{
	movf	_elenc+1,0
	subwf	_el+1,0
	btfss	STATUS,Z
	goto	_me1
	movf	_elenc,0
	subwf	_el,0
	btfss	STATUS,Z
	goto	_me1
	  }
 meoff:
  asm{
	bcf	PORTA,0
	bcf	PORTA,1
	return
	  }
 me1:
  asm{
	btfsc	STATUS,C
	goto	_me2
	bcf	_stat,6
	movf	_el+1,0
	movwf	_azenc+1
	movf	_el,0
	movwf	_azenc
	goto	_meh
	  }
 me2:
  asm{
	bsf	_stat,6
	movf	_elenc+1,0
	movwf	_azenc+1
	movf	_el+1,0
	movwf	_elenc+1
	movf	_elenc,0
	movwf	_azenc
	movf	_el,0
	movwf	_elenc
	  }
 meh:
  asm{
	comf	_azenc,1
	incf	_azenc,1
	btfsc	STATUS,Z
	decf	_azenc+1,1
	comf	_azenc+1,1
	movf	_azenc,0
	addwf	_elenc,1
	btfsc	STATUS,C
	incf	_elenc+1,1
	movf	_azenc+1,0
	addwf	_elenc+1,1
	  }
 mehist:
  asm{
	movf	_elenc+1,1
	btfss	STATUS,Z
	goto	_meon
	movf	_hist,0
	subwf	_elenc,0
	btfss	STATUS,C
	goto	_meoff
	  }
 meon:
  asm{
	btfsc	_stat,6
	goto	_mecw
	bcf	PORTA,1
	bsf	PORTA,0
	return
	  }
 mecw:
  asm{
	bsf	PORTA,1
	bcf	PORTA,0
	return
	  }
}

asm{
  __CONFIG 0x3ff9;
  __IDLOCS 0x0000;
}

/* 
   Come si fa a impostare la EEPROM in modo che l'assemblatore se ne
   accorga?
*/
