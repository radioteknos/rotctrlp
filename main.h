/*
 *   rotctrlp - antenna rotator control panel
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

/* TODO: better, link version to dev structure */
#define __VERSION "v0.1"

/* input fifo (from trk) */
#define TRKDIR ".trk/"
#define FIFOINDEFNAME "trkOUTrot"

#define BUFFLEN 128

/* 
   cmd names meaning:

   "SET" set a value
   "BACK" ask a value that can vary
   "LAST" ask the last settled value (that cannot vary)

*/
/* rotctrlp <-> naxrot2d */
#define cmdSETAZ      "AZ"            /* 'a' */
#define cmdBACKAZ     "AZ"            /* 'a' */
#define cmdLASTAZ     "LASTAZ"        /* 'i' */
#define cmdSETOFFAZ   "OFFAZ"         /* 'f' */
#define cmdLASTOFFAZ  "LASTOFFAZ"     /* 'f' */
#define cmdSETEL      "EL"            /* 'e' */
#define cmdBACKEL     "EL"            /* 'e' */
#define cmdLASTEL     "LASTEL"        /* 'j' */
#define cmdSETOFFEL   "OFFEL"         /* 'g' */
#define cmdLASTOFFEL  "LASTOFFEL"     /* 'g' */
#define cmdERROR      "ERROR"         /* 'n' */
#define cmdSETHIST    "HIST"          /* 'h' */
#define cmdLASTHIST   "LASTHIST"      /* 'h' */
#define cmdSETSTEP    "STEP"          /* 'p' */
#define cmdLASTSTEP   "LASTSTEP"      /* 'p' */
#define cmdCCW        "CCW"           /* 'l' */
#define cmdCW         "CW"            /* 'r' */
#define cmpUP         "UP"            /* 'u' */
#define cmdDOWN       "DOWN"          /* 'd' */
#define cmdQUIT       "QUIT"          /* quit naxrot2d */

/* trk -> rotctrlp */
#define cmdTRKAZ      "AZ"
#define cmdTRKEL      "EL"


void quit(int signo);
void error_beep(void), utctimestr(char *buff);
int fifo_open_create(int *fdi, int create, char *path);

void send_drv_cmd_set(char *cmd, float val);
unsigned int skipspace(char **b), skipnotspace(char **b);
void use(void);
