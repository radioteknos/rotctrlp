/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@amsat.org
 */

#include <stdio.h>       
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <sys/stat.h>
#include <sys/types.h>       
#include "daemon.h"


#define XON  17
#define XOFF 19


#define NCMD 21
const char cmdname[NCMD][10]={
  "AZ\0", "BACKAZ\0", "LASTAZ\0", "OFFAZ\0", "LASTOFFAZ\0",
  "EL\0", "BACKEL\0", "LASTEL\0", "OFFEL\0", "LASTOFFEL\0",
  "HIST\0", "LASTHIST\0", 
  "STEP\0", "LASTSTEP\0",
  "CCW\0", "CW\0", "UP\0", "DOWN\0",
  "STATUS\0", "ERROR\0", "RESET\0"
};

const char rotcmd[NCMD]={'a', 'a', 'i', 'f', 'f',
			 'e', 'e', 'j', 'g', 'g',
			 'h', 'h',
			 'p', 'p',
			 'l', 'r', 'u', 'd',
			 's', 'n', 'z'
};

const char rotcmdhasval[NCMD]={1, 0, 0, 1, 0,
			       1, 0, 0, 1, 0,
			       1, 0,
			       1, 0,
			       0, 0, 0, 0,
			       1, 1, 0
};

int ml=1;

/* Funzioni */
int serial_open(char *);
void UTCstring(double, char *, int);
double JDsyst(void);
void catch_int(int);

 
int main(int argc, char **argv){
int fdi, fdo, fdser, lastfd=0, n, i, nf, j; 
int clo, mode, create=0, verbose=0, log=0;
char buffer[256], dirpath[80], gfn[80], b2[256], time[40];
fd_set rfds;
char fifoname[2][8]={"rotin\0", "rotout\0"}, serialportname[80]="/dev/ttyS3",
     logfilename[80];
FILE *fplog;

/* Se nessuna directory e` specificata nella linea di comando allora si usa 
   la $HOME/.rotor e $HOME si ricava dall'environment */
 strcpy(dirpath, getenv("HOME"));
 if(dirpath==NULL) {
   fprintf(stderr, "Unable to find HOME dir ?!\n"); return 1;
 }
 strcat(dirpath, "/.rotor/");
 
 /* Scansione della riga di comando */
 if(argc>1) {
   while(1){
     clo=getopt(argc, argv, "d:chvs:l:");
     if(clo==-1) break;
     switch(clo){
     case 'd': if(sscanf(optarg, "%s", dirpath)==0 ) 
       { printf("Invalid path for fifos\n"); return 1; } break; 
     case 'c': create=1; break;
     case 'h':
       printf("\nRotor daemon:\n -d <dirpath> where fifos are stored\n");
       printf(" -c create fifos if they don't exist\n");
       printf(" -v print on stderr whenever data arrival\n");
       printf(" -s <serialportname> set serial port to rotor \
[default /dev/ttyS3]\n");
       printf(" -l <r|s|b> enable logfile (on start) for message to Rotor,\
Station, Both\n");
       printf(" -h this help\n\n");
       printf("String to be used to controll rotord:\n");
       printf(" \"quitrotord\" - Quit the rotord\n");
       printf(" \"logtorot\" - Enable logging commands to rotor\n");
       printf(" \"logtosta\" - Enable logging messages to station\n");
       printf(" \"logtoboth\" - Enable logging commands to both rotor and\
 station\n");
       printf(" \"logoff\" - Disable logging\n");
       return(0);
     case 'v': verbose=1; break;
     case 'l': if(sscanf(optarg, "%s", buffer)==0) {
       printf("Invalid logfile option, see help\n"); return 1;
     }
     if(buffer[0]=='r') log=1;
     else if(buffer[0]=='s') log=2;
     else if(buffer[0]=='b') log=3;
     else {
       printf("Invalid logfile option, see help\n"); return 1;
     }
     break;
     case 's': if(sscanf(optarg, "%s", serialportname)==0 ) { 
       printf("Invalid serial port name\n"); return 1; } break; 
     }
   }
 }

/* Se il rotord viene terminato con un segnale deve essere possibile 
   segnalarlo sul logfile (e comunque chiudere tutto in maniera pulita) */
signal(SIGINT, catch_int);
signal(SIGABRT, catch_int);
signal(SIGTERM, catch_int);

 /* Procedura per far diventare un daemon, se possibile */
 if(verbose==0){
   if(daemon_start(1)==0) {
     fprintf(stderr, "rotord cannot beacame a daemon\n");
     return 1;
   }
 }
 else {
   printf("Using verbose option rotord cannot be a daemon.\n");
 }
 
 /* Se le fifo mancanti devono essere create...*/
 if(create==1){
   /* innanzitutto si guarda se c'e` la directory o se la si puo` creare... */
   mode=F_OK; 
   if(access(dirpath, mode)==-1) {
     if(mkdir(dirpath, 0600)==-1) {
       fprintf(stderr, "Unable to create \"%s\"\n", dirpath); 

       return 1;
     }
   }
   /* e poi si creano, all'occorrenza, le fifo. */
   for(n=0; n<2; n++){
     mode=F_OK; strcpy(gfn, dirpath); strcat(gfn, fifoname[n]);
     if(access(gfn, mode)==-1){
       if(mkfifo(gfn, 0600)==-1){
	 printf("%s not found and unable to create it in %s\n", 
		fifoname[n], dirpath);
	 return 1;
       }
     }
   }
 }
   
   
 /* Apertura fifo */ 
   strcpy(gfn, dirpath); strcat(gfn, fifoname[0]);
   if((fdi=open(gfn, O_RDWR|O_NONBLOCK))==-1)
     {fprintf(stderr, "Unable to open input fifo \"%s\"\n",
	      gfn); return 1;}
   strcpy(gfn, dirpath); strcat(gfn, fifoname[1]);
   if((fdo=open(gfn, O_RDWR|O_NONBLOCK))==-1)
     {fprintf(stderr, "Unable to open output fifo \"%s\"\n",
	      gfn); return 1;}
   
   
   /* Apertura canale seriale e impostazioni protocollo */
   if((fdser=serial_open(serialportname))==-1)
     { fprintf(stderr, "Unable to open serial line \"%s\"\n",
	      serialportname); return 1;}

   /* Apertura logfile, anche se non richiesto, in modo da segnalare se sia
      possibile aprire il file */
   strcpy(logfilename, dirpath); strcat(logfilename, "rotorlog");
   if((fplog=fopen(logfilename, "a"))==NULL) {
     fprintf(stderr, "Unable to open logfile %s\n", logfilename); return 1;
   }
   chmod(logfilename, 0644);
     UTCstring(JDsyst(), time, 1); bzero(b2, 80);
     fprintf(fplog, "\n--    [%s]: Rotord started [PID %d]\n", 
	     time, getpid());
   fclose(fplog);
   


   /* Impostazioni per la select() */
   if(fdi>lastfd)
     lastfd=fdi;
   if(fdser>lastfd)
     lastfd=fdser;
   FD_ZERO(&rfds); 
   FD_SET(fdi, &rfds); FD_SET(fdser, &rfds);
   lastfd++;
   
   /* Ciclo principale */
   while(ml){ 
     select(lastfd, &rfds, NULL, NULL, NULL);
     
     if(FD_ISSET(fdi, &rfds)) { 
       bzero(buffer, 80);
       n=read(fdi, buffer, 79);
       if(verbose==1) fprintf(stderr, "DATA from %s: %d bytes\n",
			      fifoname[0], n);
       /* sono stati letti n byte dalla fifo di ingresso */


       for(i=0; i<n; i++) buffer[i]=toupper(buffer[i]);

       /* Decodifica comandi diretti al rotord (non devono passare al rotore)*/
       if(strncmp(buffer, "QUITROTORD\n", 11)==0) break;
       if(strncmp(buffer, "LOGTOROT\n", 9)==0) {
	 log=1; 
	 if((fplog=fopen(logfilename, "a"))==NULL) {
	   if(verbose==1) 
	     fprintf(stderr, "Unable to open logfile %s\n", logfilename);
	 }
	 else {
	   UTCstring(JDsyst(), time, 1); bzero(b2, 80);
	   fprintf(fplog, "->    [%s]: Logging commands to rotor\n", time);
	   fclose(fplog);
	 }
	 FD_ZERO(&rfds); FD_SET(fdi, &rfds); FD_SET(fdser, &rfds);
	 continue;
       }
       if(strncmp(buffer, "LOGTOSTA\n", 9)==0) {
	 log=2;
	 if((fplog=fopen(logfilename, "a"))==NULL) {
	   if(verbose==1) 
	     fprintf(stderr, "Unable to open logfile %s\n", logfilename);
	 }
	 else {
	   UTCstring(JDsyst(), time, 1); bzero(b2, 80);
	   fprintf(fplog, "->    [%s]: Logging messages to station\n", time);
	   fclose(fplog);
	 }
	 FD_ZERO(&rfds); FD_SET(fdi, &rfds); FD_SET(fdser, &rfds);
	 continue;
       }
       if(strncmp(buffer, "LOGTOBOTH\n", 10)==0) {
	 log=3;	 
	 if((fplog=fopen(logfilename, "a"))==NULL) {
	   if(verbose==1) 
	     fprintf(stderr, "Unable to open logfile %s\n", logfilename);
	 }
	 else {
	   UTCstring(JDsyst(), time, 1); bzero(b2, 80);
	   fprintf(fplog, "->    [%s]: Logging both to rotor and station\n",
		   time);
	   fclose(fplog);
	 }
	 FD_ZERO(&rfds); FD_SET(fdi, &rfds); FD_SET(fdser, &rfds);
	 continue;
       }
       if(strncmp(buffer, "LOGOFF\n", 7)==0) {
	 log=0;
	 if((fplog=fopen(logfilename, "a"))==NULL) {
	   if(verbose==1) 
	     fprintf(stderr, "Unable to open logfile %s\n", logfilename);
	 }
	 else {
	   UTCstring(JDsyst(), time, 1); bzero(b2, 80);
	   fprintf(fplog, "->    [%s]: Stop logging\n", time);
	   fclose(fplog);
	 }
	 FD_ZERO(&rfds); FD_SET(fdi, &rfds); FD_SET(fdser, &rfds);
	 continue;
       }

       /* Command string parsing */
       for(i=0; i<NCMD; i++){
	 if(strncmp(buffer, cmdname[i], strlen(cmdname[i]))==0) break;
       }
       
       /* Se e` stato riconosciuto un comando valido */
       if(i<NCMD) {

	 if(log&0x01) {
	   if((fplog=fopen(logfilename, "a"))==NULL) {
	     if(verbose==1) 
	       fprintf(stderr, "Unable to open logfile %s\n", logfilename);
	   }
	   else {
	     UTCstring(JDsyst(), time, 1); bzero(b2, 80);
	     for(j=0; j<n; j++) {
	       if(buffer[j]=='\n' || buffer[j]=='\r') continue;
	       strncat(b2, buffer+j, 1);
	     }
	     fprintf(fplog, "->ROT [%s]: %s\n", time, b2);
	     fclose(fplog);
	   }
	 }
	 
	 /* qui inizia la decodifica e l'attuazione del comando */
	 nf=1;
	 for(j=0; j<strlen(buffer); j++) {
	   if(buffer[j]==' ' || buffer[j]=='\t') { 
	     nf=2; break;
	   }
	 }
	 bzero(b2, 80);
	 strcpy(b2, buffer+j+1);
	 

	 /* Il rotore accetta comandi con e senza campo numerico */
	 if((rotcmdhasval[i]==1 && nf==2))
	   sprintf(buffer, "%c %s", rotcmd[i], b2);
	 else if((rotcmdhasval[i]==0 && nf==1))
	   sprintf(buffer, "%c\n", rotcmd[i]);
	 else {
	   if(verbose==1) 
	     printf("Command and data do not match!\n");
	   /* Senza non funziona ma come mai ci vuole?
	      La documentazione non lo dice */
	   FD_ZERO(&rfds); 
	   FD_SET(fdi, &rfds); FD_SET(fdser, &rfds);
	   continue;
	 }
   
	 /* 
	    L`implementazione della seriale sul microcontrollore non permette
	    la ricezione consecutiva di piu` caratteri, e` necessario mettere 
	    qualche decina di bit di stop
	 */
	 for(i=0; i<strlen(buffer); i++) {
	   write(fdser, buffer+i, 1); usleep(1000);
	 }

       }

       /* Se invece il comando non e` conosciuto */
       else {
	 sprintf(b2, "Unknow command from input fifo: %s", buffer);
	 write(fdo, b2, strlen(b2));
	 if(verbose==1)
	  fprintf(stderr, "%s", b2);
	 if(log&0x01) {
	   if((fplog=fopen(logfilename, "a"))==NULL) {
	     if(verbose==1) 
	       fprintf(stderr, "Unable to open logfile %s\n", logfilename);
	   }
	   else {
	     UTCstring(JDsyst(), time, 1); bzero(b2, 80);
	     fprintf(fplog, "->ROT [%s]: %s\n", time, b2);
	     fclose(fplog);
	   }
	 }
       }



       /* Senza non funziona ma come mai ci vuole?
	  La documentazione non lo dice */
       FD_ZERO(&rfds); 
       FD_SET(fdi, &rfds); FD_SET(fdser, &rfds);
       continue;
     }

     if(FD_ISSET(fdser, &rfds)) {
       n=read(fdser, buffer, 79);
       if(verbose==1) fprintf(stderr, "DATA from serial line %s: %d bytes\n",
			      serialportname, n);

       /* sono stati letti n byte dal canale seriale */

       if(log&0x02) {
	 if((fplog=fopen(logfilename, "a"))==NULL) {
	   if(verbose==1) 
	     fprintf(stderr, "Unable to open logfile %s\n", logfilename);
	 }
	 else {
	   UTCstring(JDsyst(), time, 1); bzero(b2, 80);
	   for(i=0; i<n; i++) {
	     if(buffer[i]=='\n' || buffer[i]=='\r') continue;
	     strncat(b2, buffer+i, 1);
	   }
	   fprintf(fplog, "<-ROT [%s]: %s\n", time, b2);
	   fclose(fplog);
	 }
       }

       while(buffer[0]==XON || buffer[0]==XOFF) 
	 for(i=0; i<strlen(buffer)-1; i++)
	   buffer[i]=buffer[i+1];

       for(i=0; i<NCMD; i++) {
	 if(buffer[0]==toupper(rotcmd[i])) break;
       }

       if(i<NCMD) {
	 strcpy(b2, cmdname[i]); strncat(b2, buffer+1, n-1);
	 /*	 sprintf(b2, "%s %s", cmdname[i], buffer+1); */
	 write(fdo, b2, strlen(b2));
       }
       else {
	 strcpy(b2, "Undecoded string from rotor: ");
	 for(i=0; i<n; i++) {
	   if(buffer[i]=='\n' || buffer[i]=='\r') continue;
	   strncat(b2, buffer+i, 1);
	 }
	 strcat(b2, "\n");
	 write(fdo, b2, strlen(b2));
       }

       /* Senza non funziona ma come mai ci vuole?
	  La documentazione non lo dice */
       FD_ZERO(&rfds); 
       FD_SET(fdi, &rfds); FD_SET(fdser, &rfds);
       continue;
     }   
   }

   if((fplog=fopen(logfilename, "a"))==NULL) {
     if(verbose==1) 
       fprintf(stderr, "Unable to open logfile %s\n", logfilename);
   }
   else {
     UTCstring(JDsyst(), time, 1); bzero(b2, 80);
     fprintf(fplog, "--    [%s]: Rotord terminated [PID %d]\n", 
	     time, getpid());
     fclose(fplog);
   }
   
   close(fdser); close(fdo); close(fdi);
   fprintf(stderr,"\n\nRotord terminated [PID %d]\n\n", getpid());
   return 0;
 }

int serial_open(char *sfn)
{
int fd;
struct termios sersettings;

 if((fd=open(sfn, O_RDWR|O_NOCTTY|O_NDELAY))!=-1) {
   bzero(&sersettings, sizeof(sersettings));
   sersettings.c_cflag=B9600|CS8|CLOCAL|CREAD;
   sersettings.c_iflag=IGNPAR | IXON | IXOFF |IGNBRK;
   sersettings.c_oflag=0;
   sersettings.c_lflag=ICANON;
   sersettings.c_cc[VTIME]=0;
   sersettings.c_cc[VMIN]=0;
   tcflush(fd, TCIOFLUSH);
   tcsetattr(fd,TCSANOW,&sersettings);
 }

 return fd;
}


void catch_int(int sign){

ml=0;
}
