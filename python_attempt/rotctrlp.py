#!/usr/bin/env python3

# rxvt 32x16

import curses, select, time, sys, getopt, shutil, curses.textpad, subprocess
import select, socket, signal

# global vars 
__VERSION="v0.1.0"
stdscr=''
statwin=''
ml=1
host="127.0.0.1"
# these ports are swapped respect to the definition in naxrotd
port_in=12324  # TBC
port_out=12323 # TBC

port_trk=12325 # TBC
az=0.
el=0.
setaz=0.
setel=0.
backend_name=""
utc=0x1;

def cmd_backaz(v):
    global az
    try:
        az=float(v)
    except ValueError:
        return
    az_update(az)

def cmd_backel(v):
    global el
    try:
        el=float(v)
    except ValueError:
        return        
    el_update(el)

def cmd_backlastaz(v):
    global setaz
    try:
        setaz=float(v)
    except ValueError:
        return
    setaz_update(setaz)

def cmd_backlastel(v):
    global setel
    try:
        setel=float(v)
    except ValueError:
        return
    setel_update(setel)

def cmd_backoffaz(v):
    # TODO better?
    statwin_update("BACKOFFAZ: %s" % v[:6])
    
def cmd_backoffel(v):
    # TODO better?
    statwin_update("BACKOFFEL: %s" % v[:6])

def cmd_backhist(v):
    # TODO (could be enough doing so?)
    statwin_update("BACKHIST: %s" % v[:7])

def cmd_backstep(v):
    # TODO (could be enough doing so?)
    statwin_update("BACKSTEP: %s" % v[:7])

def cmd_backstat(v):
    statwin_update("stat: %s" % v[:11])

def cmd_goterr(v):
    global statwin
    errno=int(float(v))
    # TODO: decode errors
    statwin_update("err: %s" % errno)

def cmd_gotreset():
    statwin_update("got RESET")

def cmd_trkaz(v):
    global setaz
    try:
        setaz=float(v)
    except ValueError:
        return        
    setaz_update(setaz)
    if(backend_name!=""):
        backend_send("AZ %5.1f\n" % setaz)

    
def cmd_trkel(v):
    global setel
    try:
        setel=float(v)
    except ValueError:
        return        
    setel_update(setel)
    if(backend_name!=""):
        backend_send("EL %5.1f\n" % setel)


    
backcmd={
    "BACKAZ": cmd_backaz, "LASTAZ": cmd_backlastaz,
    "BACKEL": cmd_backel, "LASTEL": cmd_backlastel,
    "LASTOFFAZ": cmd_backoffaz, "LASTOFFEL": cmd_backoffel,    
    "HIST": cmd_backhist,
    "LASTSTEP": cmd_backstep,    
    "STATUS": cmd_backstat,
    "ERROR": cmd_goterr,
    # handled separately
    #    "RESET": cmd_gotreset
}

trkcmd={
    "AZ": cmd_trkaz, 
    "EL": cmd_trkel
    }


def rotctrlp_tui(dummy):
    global stdscr, statwin, __VERSION, ml, backend_name, utc
    global host, port_in, port_out
    global az, el, setaz, setel

    # var init
    ARR_TO=3    # arrow flash timeout (in 100ms unit) plus one
    PRESCBAE=24  # timed backaz/backel request (in 100ms unit)

    presc_backazel=PRESCBAE
    rsbuff=""
    arrow_to=0
    ext_key=0
    boot_msg=""
    
    # catch ctrl+C
    signal.signal(signal.SIGINT, signal_handler)

    # rx socket init
    skrx=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    skrx.bind((host,port_in))
    skrx.listen(0)

    # trk socket init
    sktrk=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sktrk.bind((host,port_trk))
    sktrk.listen(0)

    # check for backend
    sktx=socket.socket()
    try:
        sktx.connect((host, port_out))
        sktx.close()
        backend_name="naxrotd"
    except ConnectionRefusedError:
        try:
            print("trying to launch naxrotd...", end='')
            sys.stdout.flush()
            # subprocess.Popen(["./naxrotd", "-d", "/dev/ttyUSB0"])
            subprocess.Popen(["naxrotd"])
            time.sleep(2)
            sktx=socket.socket()
            try:
                sktx.connect((host, port_out))
                sktx.close()
                backend_name="naxrotd"
                print("done");
            except ConnectionRefusedError:
                boot_msg="unable to connect to naxrotd!"
                print(boot_msg)
                backend_name=""
        except FileNotFoundError:
            boot_msg="naxrotd nor found!"
            print(boot_msg)
            backend_name=""

    if(backend_name==""):
        time.sleep(2)
    else:
        time.sleep(0.5)
        
    # ncurses init
    stdscr=curses.initscr()
    curses.curs_set(0) # invisible cursor
    curses.cbreak()
    curses.noecho()
    stdscr.timeout(0)
    curses.start_color()

    curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK);
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK);
    curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_BLACK);
    curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_CYAN);
    curses.init_pair(5, curses.COLOR_RED, curses.COLOR_BLACK);
    curses.init_pair(6, curses.COLOR_CYAN, curses.COLOR_BLACK);
    curses.init_pair(7, curses.COLOR_BLUE, curses.COLOR_CYAN);
    curses.init_pair(8, curses.COLOR_BLACK, curses.COLOR_YELLOW);

    statwin, remcmdwin=tui_init(backend_name)
    statwin.addstr(time.strftime("--- start %H:%M:%SZ ---", time.gmtime()))
    if(boot_msg!=""):
        statwin.addstr("\n%s" % boot_msg)
    statwin.refresh()
        
    # var init from file or reading back
    # TODO: get backEZ, backEL to set az/el
    #
    setaz=az
    setel=el
    step=5.0 # TODO: readback
    hist=2.0 # TODO: readback
    
    # ncurses i/f finalizing
    az_update(az)
    el_update(el)
    setaz_update(setaz)
    setel_update(setel)

    arrow_left_show(0)
    arrow_up_show(0)
    arrow_rigth_show(0)
    arrow_down_show(0)

    # select init
    inp=[skrx, sktrk]
    outp=[]
    err=[]

    # main loop
    while ml:
        rr, wr, ex=select.select(inp, outp, err, 0.100)

        # got select timeout, so check ncurses kbd
        if(rr==[]):
            kbd=stdscr.getch()
            # TODO: remove (DEBUG)
            # if(kbd!=-1):
            #     stdscr.addstr(1, 10, "%3d" % kbd)

            if(kbd==ord('q') and ext_key==0):
                break

            elif(kbd==ord('z') and ext_key==0):
                utc=utc^0x1
                
            elif(kbd==ord('a') and ext_key==0): # set_AZ
                tmpaz, valid=set_az()
                if(valid):
                    setaz=tmpaz
                    if(backend_name!=""):
                        backend_send("AZ %5.1f\n" % setaz)
                            
                else:
                    print("\a", end="")
                    sys.stdout.flush()
                setaz_update(setaz)

            elif(kbd==ord('e') and ext_key==0): # set_EL
                tmpel, valid=set_el()
                if(valid):
                    setel=tmpel
                    if(backend_name!=""):
                        backend_send("EL %4.1f\n" % setel)
                else:
                    print("\a", end="")
                    sys.stdout.flush()
                setel_update(setel)

            elif(kbd==ord('s') and ext_key==0):
                tmpstep, valid=set_step()
                if(valid):
                    step=tmpstep
                    if(backend_name!=""):
                        backend_send("STEP %4.1f\n" % step)
                else:
                    print("\a", end="")
                    sys.stdout.flush()

            elif(kbd==ord('h') and ext_key==0):
                tmphist, valid=set_hist()
                if(valid):
                    hist=tmphist
                    if(backend_name!=""):
                        backend_send("HIST %4.1f\n" % hist)
                else:
                    print("\a", end="")
                    sys.stdout.flush()
            
            elif(kbd==393 and ext_key==0):      # shift+arrow left, set_AZ--
                setaz=az-step
                if(setaz<0.):
                    setaz=0.
                else:
                    arrow_to=ARR_TO
                    arrow_left_show(1)
                setaz_update(setaz)
                if(backend_name!=""):
                    backend_send("AZ %5.1f\n" % setaz)
            elif(kbd==402 and ext_key==0):      # shift+arrow rigth, set_AZ++
                setaz=az+step
                if(setaz>=360.):
                    setaz=360.
                else:
                    arrow_to=ARR_TO
                    arrow_rigth_show(1)
                setaz_update(setaz)
                if(backend_name!=""):
                    backend_send("AZ %5.1f\n" % setaz)
            elif((kbd==511 or kbd==513) and ext_key==0): # shift+arrow down,
                                                        # set_EL--
                setel=el-step
                if(setel<0.):
                    setel=0.
                else:
                    arrow_to=ARR_TO
                    arrow_down_show(1)
                setel_update(setel)
                if(backend_name!=""):
                    backend_send("EL %4.1f\n" % setel)
            elif((kbd==518 or kbd==529) and ext_key==0): # shift+arrow up,
                                                        # set_EL++
                setel=el+step
                if(setel>=90.):
                    setel=90.
                else:
                    arrow_to=ARR_TO
                    arrow_up_show(1)
                setel_update(setel)
                if(backend_name!=""):
                    backend_send("EL %4.1f\n" % setel)
            elif(kbd==27):                      # escaped keys
                ext_key=1

            elif(kbd==99 and ext_key==1):       # config sub menu
                # TODO: config
                print("TODO_CFG")
            elif(kbd==104 and ext_key==1):      # help submenu
                # TODO: config
                print("TODO_HELP")
            
            if(kbd!=27):
                ext_key=0

        # got stuff from sockets   
        else:
            # stdscr.addstr(4, 2, "%s" % rv)
            for s in rr:
                # print(s.getsockname())
                if(s==skrx):
                    clirx,clirx_addr=skrx.accept()
                    inp.append(clirx)
                elif(s==sktrk):
                    clitrk,clitrk_addr=sktrk.accept()
                    inp.append(clitrk)
                elif(s.getsockname()[1]==port_in):           
                    try:
                        data=s.recv(256).decode().strip().split()
                    except ConnectionResetError:
                        data=[]
                    if(data):
                        stdscr.addstr(3, 2, "%s" % data)
                        if(len(data)>=2):
                            try:
                                backcmd[data[0]](data[1])
                            except KeyError:
                                try:
                                    msg="!err: ".join(data)[:11]
                                    # statwin.addstr("\n!err: %s" %
                                    #                "".join(data)[:18])
                                except:
                                    msg=" "
                                statwin_update(msg.strip('\x00'))
                        elif(len(data)==1):
                            if(data[0].upper()=="RESET"):
                                cmd_gotreset()
                    else:
                        inp.remove(s)
                        s.close()
                elif(s.getsockname()[1]==port_trk):
                    try:
                        data=s.recv(256).decode().strip().split()
                    except ConnectionResetError:
                        data=[]
                    if(data):
                        # stdscr.addstr(1, 2, "%s" % data)
                        if(len(data)>=2):
                            try:
                                trkcmd[data[0].upper()](data[1])
                                remcmdwin.addstr("\n%s" %
                                               " ".join(data)[:13])
                                remcmdwin.refresh()

                            except KeyError:
                                try:
                                    msg="!err: ".join(data)[:11]
                                except:
                                    msg="!err:"
                                statwin_update(msg)
                    else:
                        inp.remove(s)
                        s.close()


        
        # other timed tasks
        # clock
        if(utc==0x1):    
            t=time.strftime("%H:%M:%SZ", time.gmtime())
        else:
            t=time.strftime("%H:%M:%S ", time.localtime())
        stdscr.addstr(10, 11, "%s" % t)
        stdscr.refresh()
        
        # arrow flash timeout
        if(arrow_to==1):
            arrow_left_show(0)
            arrow_up_show(0)
            arrow_rigth_show(0)
            arrow_down_show(0)
        if(arrow_to>0):
            arrow_to-=1

        # backaz/backel continuos request
        if(presc_backazel==PRESCBAE//2):
            backend_send("BACKAZ")
            presc_backazel-=1
        elif(presc_backazel==0):
            backend_send("BACKEL")
            presc_backazel=PRESCBAE
        else:
            presc_backazel-=1
            
    # ncurses end
    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()
# main end

def backend_send(msg):
    global host, port_out, backend_name

    try:
        s=socket.socket()
        s.connect((host, port_out))
    except ConnectionRefusedError:
        backend_name=""
        backend_update(backend_name)
        return(-1)

    s.send(msg.encode())
    s.close()

    return 0

def backend_update(name):
    global stdscr
    
    if(name!=""):
        stdscr.addstr(10, 5, "B", curses.color_pair(5)+curses.A_BOLD)
    else:
        stdscr.addstr(10, 5, " ")
    stdscr.addstr(0, 17, "%s" % name, curses.color_pair(6))

def configure():
    print("TODO: config")
#config end    

def tui_init(backend_name):
    # head
    stdscr.addstr(0, 0, "ROTCTRLP %s" %
                  __VERSION, curses.color_pair(5)+
                  curses.A_BOLD)

    # available services/options/add-ons
    stdscr.addstr(10, 1, "[      ]", curses.color_pair(5)+curses.A_BOLD)
    if(shutil.which('rotmap') is not None):
        stdscr.addstr(10, 3, "M", curses.color_pair(5)+curses.A_BOLD)
    # backend
    backend_update(backend_name)

    stdscr.addstr(0, 0, "ROTCTRLP %s" %
                  __VERSION, curses.color_pair(5)+
                  curses.A_BOLD)
    
    stdscr.addstr(10, 21, "STAT", curses.color_pair(7)+curses.A_BOLD)
    statwin=curses.newwin(5, 32, 11, 0)
    statwin.bkgd(' ', curses.color_pair(4))
    statwin.scrollok(True)
    statwin.refresh()

    stdscr.addstr(5, 14, " REMCMD", curses.color_pair(8)+curses.A_BOLD)
    remcmdwin=curses.newwin(5, 14, 5, 0)
    remcmdwin.bkgd(' ', curses.color_pair(8))
    remcmdwin.scrollok(True)
    remcmdwin.refresh()

    stdscr.refresh()

    return statwin, remcmdwin
    

def az_update(az):
    stdscr.addstr(2, 1, "AZ %5.1f" % az, curses.color_pair(2)+curses.A_BOLD)
    
def el_update(el):
    stdscr.addstr(2, 11, "EL %5.1f" % el, curses.color_pair(2)+curses.A_BOLD)

def setaz_update(saz):
    stdscr.addstr(4, 0, "sAZ %5.1f" % saz, curses.color_pair(1))

def setel_update(sel):
    stdscr.addstr(4, 10, "sEL %5.1f" % sel, curses.color_pair(1))

def arrow_up_show(is_on):
    if(is_on):
        stdscr.addstr(6, 17, u'\u2191'.encode('utf-8'), curses.color_pair(2)+
                      curses.A_BOLD)
    else:        
        stdscr.addstr(6, 17, u'\u2191'.encode('utf-8'), curses.color_pair(1))

def arrow_down_show(is_on):
    if(is_on):
        stdscr.addstr(8, 17, u'\u2193'.encode('utf-8'), curses.color_pair(2)+
                      curses.A_BOLD)
    else:        
        stdscr.addstr(8, 17, u'\u2193'.encode('utf-8'), curses.color_pair(1))

def arrow_left_show(is_on):
    if(is_on):
        stdscr.addstr(7, 16, u'\u2190'.encode('utf-8'), curses.color_pair(2)+
                      curses.A_BOLD)
    else:        
        stdscr.addstr(7, 16, u'\u2190'.encode('utf-8'), curses.color_pair(1))

def arrow_rigth_show(is_on):
    if(is_on):
        stdscr.addstr(7, 18, u'\u2192'.encode('utf-8'), curses.color_pair(2)+
                      curses.A_BOLD)
    else:        
        stdscr.addstr(7, 18, u'\u2192'.encode('utf-8'), curses.color_pair(1))

def set_az():
    curses.echo()
    inpwin=curses.newwin(1, 6, 4, 4)
    inpwin.addstr(0, 0, "_____", curses.color_pair(4))
    valid=1
    try:
        v=float(inpwin.getstr(0, 0))
    except ValueError:
        valid=0
        v=0.
    curses.noecho()
    if(v<0. or v>360.):
        valid=0
    inpwin.addstr(0, 0, "")
    return(v, valid)

def set_el():
    curses.echo()
    inpwin=curses.newwin(1, 5, 4, 15)
    inpwin.addstr(0, 0, "____", curses.color_pair(4))
    valid=1
    try:
        v=float(inpwin.getstr(0, 0))
    except ValueError:
        valid=0
        v=0.
    curses.noecho()
    if(v<0. or v>90.):
        valid=0
    return(v, valid)

def set_step():
    curses.echo()
    inpwin=curses.newwin(1, 10, 3, 5)
    inpwin.addstr(0, 0, "STEP ____", curses.color_pair(4))
    valid=1
    try:
        v=float(inpwin.getstr(0, 5))
    except ValueError:
        valid=0
        v=0.
    curses.noecho()
    if(v<0. or v>90.):
        valid=0
    inpwin.addstr(0, 0, "         ", curses.color_pair(1))
    inpwin.refresh()
    return(v, valid)

def set_hist():
    curses.echo()
    inpwin=curses.newwin(1, 10, 3, 5)
    inpwin.addstr(0, 0, "HIST ____", curses.color_pair(4))
    valid=1
    try:
        v=float(inpwin.getstr(0, 5))
    except ValueError:
        valid=0
        v=0.
    curses.noecho()
    if(v<0. or v>10.):
        valid=0
    inpwin.addstr(0, 0, "         ", curses.color_pair(1))
    inpwin.refresh()
    return(v, valid)

def statwin_update(msg):
    global utc
    
    if(utc==0x1):    
        t=time.strftime("%H:%M:%SZ", time.gmtime())
    else:
        t=time.strftime("%H:%M:%S ", time.localtime())
    try:
        statwin.addstr("\n%s %s" % (t, msg))
    except ValueError:
        pass
    statwin.refresh()
    

def use():
    print("\nRotor Control Pannel (rotctrlp) %s (c) by Lapo Pieri 2000-2019 under GNU/GPL\n" % __VERSION)
    print("  -h --help (this) help");
    print("  -v print version number\n");
    

def signal_handler(signal, frame):
    global ml
    ml=0
    
if __name__=="__main__":
    # command line params scan
    # trk can be used interactive or batch with or without ncurses
    # so a launcher is placed here instead of in a main()
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], 'hvkp', ['help',
                                                              'version'
        ])
    except getopt.GetoptError:
        use()
        sys.exit(1)
        
        
    for opt, arg in opts:
        if opt in ("-h", "--help"):
	        use()
	        sys.exit(0)
        if opt in ("-v", "--version"):
            print("%s" % __VERSION)
            sys.exit(0)
            
    # so if code crash terminal is restored to echo and standard stuff
    # with non need to issue a $ reset at every crash
    curses.wrapper(rotctrlp_tui)
