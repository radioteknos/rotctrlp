#!/usr/bin/env python3
import signal, sys, socket

host="127.0.0.1"
port=12325 # TBC

def main():
    # catch ctrl+C
    signal.signal(signal.SIGINT, signal_handler)

    while True:
        with open("/home/lapo/.trk/trkOUTrot", "r") as trkpipe:
            for lb in trkpipe:
                # print(lb)
                s=socket.socket()
                try:
                    s.connect((host, port))
                    s.send(lb.encode())
                    s.close()
                except:
                    pass

def signal_handler(signal, frame):
    print("\ntrk2rot quit")
    sys.exit(0)

    
if __name__=="__main__":
   main()
