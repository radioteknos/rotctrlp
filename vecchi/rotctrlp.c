/*
 * TRK - Satellite tracking program based on Norad SGP/SDP model with
 *       curses interface
 *
 *	by Lapo Pieri IK5NAX  2000-2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to ik5nax@amsat.org
 */

/*
  La trasmissione per la richiesta periodica di aggiornamento dovrebbe essere
  fatta dinamica, cioe` dipendente dalla differenza fra lastaz e backaz, in
  modo da non stressare il rotore quando non ce n'e` bisogno e da inoltrare
  richieste fitte quando ci sono delle variazioni in modo da far vedere 
  la variazione ben particolareggiata; e cose simili...
*/

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
 

#define PRINTAZ mvwprintw(pw, 1,7, "AZ %6.2f", az)
#define PRINTEL mvwprintw(pw, 1,22, "EL %6.2f", el)
#define PRINTLASTAZ mvwprintw(pw, 2,3, "lastAZ %6.2f", lastaz)
#define PRINTLASTEL mvwprintw(pw, 2,18, "lastEL %6.2f", lastel)
#define PRINTHIST mvwprintw(pw, 1,66, "Hist %6.2f", hist)
#define PRINTSTEP mvwprintw(pw, 2,66, "Step %6.2f", step)
#define PRINTOFFAZ mvwprintw(pw, 2,50, "offAZ  %6.2f", offaz)
#define PRINTOFFEL mvwprintw(pw, 1,50, "offEL  %6.2f", offel)
#define SHOWDOWN mvwprintw(pw, 1,33, "DOWN");
#define SHOWEL0 mvwprintw(pw, 1,38, "EL OFF");
#define SHOWUP mvwprintw(pw, 1,45, "UP");
#define SHOWCCW mvwprintw(pw, 2,34, "CCW");
#define SHOWAZ0 mvwprintw(pw, 2,38, "AZ OFF");
#define SHOWCW mvwprintw(pw, 2,45, "CW");
#define UNSHOW mvwprintw(pw, 1,33, "               "); mvwprintw(pw, 2,33, "               ")

#define FIFOINNAME "rctrlpi"
#define FIFOOUTNAME "rctrlpo"
#define ROTORDIR ".rotor/"

#define XON  17
#define XOFF 19

#define NOLOG 0
#define LOGTOROT 1
#define LOGTOSTAT 2
#define LOGTOBOTH 3

WINDOW *pw, *ew;
#define nERR 12
const char errname[nERR][40]={"Serial Error\0", "???\0",
			      "Uncomplete/wrong serial string\0",
			      "Value out of range\0", "Unknown Command\0", 
			      "Unable to execute command\0", "???\0", "???\0",
			      "Endstroke\0", "Overload AZ\0", 
			      "Overload EL\0", "Overheat\0"};


#define NCMD 21
const char cmdname[NCMD][10]={
  "AZ\0", "BACKAZ\0", "LASTAZ\0", "OFFAZ\0", "LASTOFFAZ\0",
  "EL\0", "BACKEL\0", "LASTEL\0", "OFFEL\0", "LASTOFFEL\0",
  "HIST\0", "LASTHIST\0", 
  "STEP\0", "LASTSTEP\0",
  "CCW\0", "CW\0", "UP\0", "DOWN\0",
  "STATUS\0", "ERROR\0", "RESET\0"
};

const char rotcmd[NCMD]={'a', 'a', 'i', 'f', 'f',
			 'e', 'e', 'j', 'g', 'g',
			 'h', 'h',
			 'p', 'p',
			 'l', 'r', 'u', 'd',
			 's', 'n', 'z'
};

struct timeoutcounter {
  int toaz;
  int toel;
  int tolastaz;
  int tolastel;
  int tooffaz;
  int tooffel;
  int tohist;
  int tostep;
  int tostat;
};

void set_az(char *os), set_el(char *os), set_step(char *os), 
  set_hist(char *os), set_offaz(char *os), set_offel(char *os);
int open_fifo(int *, int *, int, char *);


int main(int argc, char **argv){
  int clo, l=1, create=0, n, i, lastfd, en, status, rotordlog=NOLOG;
  int fdi, fdo;
  float az, el, lastaz, lastel, hist, step, offaz, offel, val;
  char buffer[80], dirpath[80]="\0";
  fd_set rfds;
  struct timeval timeout;
  struct timeoutcounter tocount, tocmax;  

  /* Se e` tutto zero e` MOLTO probabile che ancora non sia stato ricevuto
     nulla dal rotore, quindi l'indicazione e` significativa */
  az=0; el=0; lastaz=0; lastel=0; hist=0; step=0; offaz=0; offel=0;

  /* Azzeramento dei timeout e impostazione soglie*/
  tocount.toaz=0; tocount.toel=0; tocount.tolastaz=0; tocount.tolastel=0;
  tocount.tooffaz=0; tocount.tooffel=0; tocount.tohist=0; tocount.tostep=0;
  tocount.tostat=0;
  tocmax.toaz=3; tocmax.toel=4; tocmax.tolastaz=5; tocmax.tolastel=6;
  tocmax.tooffaz=50; tocmax.tooffel=51; tocmax.tohist=10; tocmax.tostep=11;
  tocmax.tostat=7; /* L'unita` sono 200ms */

  /* Scansione della riga di comando */
  if(argc>1) {
    while(1){
      clo=getopt(argc, argv, "cd:h?");
      if(clo==-1) break;
      switch(clo){
      case 'c': create=1; break;
      case 'd': sscanf(optarg, "%s", dirpath); break;
      case 'h':
      case '?':
	printf("\nRotor Control Pannel:\n\n");
	printf(" -c create fifos if they don't exist \n");
	printf(" -d <dirpath> where the fifos are located (default ~/.rotor)\n"
	       );
	return(0);
      }
    }
  }

  /* Apertura delle fifo */
  if(open_fifo(&fdi, &fdo, create, dirpath)==-1) return 1;


  /* Impostazioni per la select() */
  lastfd=fdi; lastfd++;
  FD_ZERO(&rfds); FD_SET(fdi, &rfds);
  timeout.tv_sec=0;
  timeout.tv_usec=200000;
 

  /* Inizializzazione delle ncurses */
  initscr(); cbreak(); noecho(); timeout(0); start_color(); curs_set(0);
  init_pair(1, COLOR_WHITE, COLOR_BLACK);
  init_pair(2, COLOR_RED, COLOR_BLACK);
  init_pair(3, COLOR_BLACK, COLOR_CYAN);

  /* Schermata */
  if((pw=newwin(9, 80, 0, 0))==NULL) {
    fprintf(stderr, "Unable to show controll pannel\n");
    return 1;
  }
  wattron(pw, COLOR_PAIR(1)); wattroff(pw, A_BOLD);
  box(pw, ACS_VLINE, ACS_HLINE); getch();
  mvwprintw(pw, 0,34, " ROTCTRLP ");
  wrefresh(pw);
  if((ew=newwin(4, 39, 4, 40))==NULL) {
    fprintf(stderr, "Unable to show controll pannel\n");
    return 1;
  }
  scrollok(ew, 1); wattron(ew, COLOR_PAIR(3)); wattron(ew, A_BOLD);
  wrefresh(ew);


  while(l){

    PRINTAZ; PRINTEL; PRINTLASTAZ; PRINTLASTEL; 
    PRINTHIST; PRINTSTEP; PRINTOFFAZ; PRINTOFFEL;
    wrefresh(pw);

    if(select(lastfd, &rfds, NULL, NULL, &timeout)) {     /* E` un file che ha
							    dati */
      if(FD_ISSET(fdi, &rfds)){
	n=read(fdi, buffer, 79);

	buffer[n+1]=0;
	for(i=0; i<n; i++) buffer[i]=toupper(buffer[i]);

	while(buffer[0]==XON || buffer[0]==XOFF || buffer[0]==' ')
	  for(i=1; i<n; i++)
	    buffer[i-1]=buffer[i];

	for(i=0; i<NCMD; i++) 
	  if(strncmp(buffer, cmdname[i], strlen(cmdname[i]))==0) break;

	
	if(i<NCMD) {
       
	  switch(rotcmd[i]) {
	  case 'a': sscanf(buffer+strlen(cmdname[i]), "%f", &az); break;
	  case 'e': sscanf(buffer+strlen(cmdname[i]), "%f", &el); break;
	  case 'i': sscanf(buffer+strlen(cmdname[i]), "%f", &lastaz); break;
	  case 'j': sscanf(buffer+strlen(cmdname[i]), "%f", &lastel); break;
	  case 'h': sscanf(buffer+strlen(cmdname[i]), "%f", &hist); break;
	  case 'p': sscanf(buffer+strlen(cmdname[i]), "%f", &step); break;
	  case 'f': sscanf(buffer+strlen(cmdname[i]), "%f", &offaz); break;
	  case 'g': sscanf(buffer+strlen(cmdname[i]), "%f", &offel); break;
	    
	  case 'n':
	    if(sscanf(buffer+strlen(cmdname[i])+1, "%d", &en)==0) break;
	    if(en>=nERR || en<0) 
	      wprintw(ew, "Unknown error: %d\n", en);
	    else
	      wprintw(ew, "%s\n", errname[en]);
	    wrefresh(ew);
	    break;
	    
	  case 's': UNSHOW;
	    sscanf(buffer+strlen(cmdname[i])+1, "%f", &val); 
	    status=(int)(val*10.);
	    if(status&0x01) { SHOWCCW; }
	    if(status&0x02) { SHOWCW; }
	    if((status&0x03)==0) { SHOWAZ0; }
	    if(status&0x04) { SHOWDOWN; }
	    if(status&0x08) { SHOWUP; }
	    if((status&0x0c)==0) { SHOWEL0; }
	    
	    wrefresh(pw);
	    break;
	    
	    
	    
	  case 'l':
	  case 'r':
	  case 'u':
	  case 'd':
	  default:
	    continue;
	  }
	}

      }

      FD_ZERO(&rfds); FD_SET(fdi, &rfds); continue; 
    }

    /* 
       Si puo` usare l'fd dello stdin (0) nella select anche se il controllo
       e` delle ncurses? Cosi` si eviterebbe il timeout.
    */
    else {                                                
      /* 
	 C'e` stato un timeout e quindi si legge la tastiera e si inviano le
	 richieste di stato e posizione con l giusta periodicita`
      */
      switch(getch()){
      case 27: { if(getch()==91) 
	switch(getch()){
	case 68: strcpy(buffer, "ccw\n"); write(fdo, buffer, strlen(buffer));
	  break; /* curs sx */
	case 67: strcpy(buffer, "cw\n"); write(fdo, buffer, strlen(buffer));
	  break; /* curs dx */
	case 65: strcpy(buffer, "up\n"); write(fdo, buffer, strlen(buffer));
	  break; /* curs up */
	case 66: strcpy(buffer, "down\n"); write(fdo, buffer, strlen(buffer));
	  break; /* curs down */
	}
      } break;
      
      case's': tocount.tostep=tocmax.tostep; 
        set_step(buffer); write(fdo, buffer, strlen(buffer)); break;
      case'h': tocount.tohist=tocmax.tohist;
        set_hist(buffer); write(fdo, buffer, strlen(buffer)); break;
      case'a': set_az(buffer); write(fdo, buffer, strlen(buffer)); break;
      case'e': set_el(buffer); write(fdo, buffer, strlen(buffer)); break;
      case'o': tocount.tooffaz=tocmax.tooffaz;
        set_offaz(buffer); write(fdo, buffer, strlen(buffer)); break;
      case'i': tocount.tooffel=tocmax.tooffel;
        set_offel(buffer); write(fdo, buffer, strlen(buffer)); break;
      case 'l': rotordlog++;
	switch(rotordlog&0x03){
	case NOLOG: strcpy(buffer, "logoff\n");
	  mvwprintw(pw, 6, 2, "          "); 
	  mvwprintw(pw, 6, 2, "%s", buffer); wrefresh(pw);
	  write(fdo, buffer, strlen(buffer)); break;
	case LOGTOROT: strcpy(buffer, "logtorot\n"); 
	  mvwprintw(pw, 6, 2, "%s", buffer); wrefresh(pw);
	  write(fdo, buffer, strlen(buffer)); break;
	case LOGTOSTAT: strcpy(buffer, "logtosta\n"); 
	  mvwprintw(pw, 6, 2, "%s", buffer); wrefresh(pw);
	  write(fdo, buffer, strlen(buffer)); break;
	case LOGTOBOTH: strcpy(buffer, "logtoboth\n"); 
	  mvwprintw(pw, 6, 2, "%s", buffer); wrefresh(pw);
	  write(fdo, buffer, strlen(buffer)); break;
	  } break;
      
      case 'q': l=0; break;
      }

      /* Invio comandi (periodici) richiesta stato e posizione */
      tocount.toaz++; tocount.toel++; tocount.tolastaz++; tocount.tolastel++;
      tocount.tooffaz++; tocount.tooffel++; tocount.tohist++; tocount.tostep++;
      tocount.tostat++;

      if(tocount.toaz>tocmax.toaz) { 
	tocount.toaz=0; strcpy(buffer, "backaz\n\0"); 
	write(fdo, buffer, strlen(buffer));
      }
      else if(tocount.toel>tocmax.toel) { 
	tocount.toel=0; strcpy(buffer, "backel\n"); 
	write(fdo, buffer, strlen(buffer));
      }
      else if(tocount.tolastaz>tocmax.tolastaz) { 
	tocount.tolastaz=0; strcpy(buffer, "lastaz\n"); 
	write(fdo, buffer, strlen(buffer));
      }
      else if(tocount.tolastel>tocmax.tolastel) { 
	tocount.tolastel=0; strcpy(buffer, "lastel\n"); 
	write(fdo, buffer, strlen(buffer));
      }
      else if(tocount.tooffaz>tocmax.tooffaz) { 
	tocount.tooffaz=0; strcpy(buffer, "lastoffaz\n"); 
	write(fdo, buffer, strlen(buffer));
      }
      else if(tocount.tooffel>tocmax.tooffel) { 
	tocount.tooffel=0; strcpy(buffer, "lastoffel\n"); 
	write(fdo, buffer, strlen(buffer));
      }
      else if(tocount.tohist>tocmax.tohist) { 
	tocount.tohist=0; strcpy(buffer, "lasthist\n"); 
	write(fdo, buffer, strlen(buffer));
      }
      else if(tocount.tostep>tocmax.tostep) { 
	tocount.tostep=0; strcpy(buffer, "laststep\n"); 
	write(fdo, buffer, strlen(buffer));
      }
      else if(tocount.tostat>tocmax.tostat) { 
	tocount.tostat=0; strcpy(buffer, "getstat\n"); 
	write(fdo, buffer, strlen(buffer));
      }

      timeout.tv_sec=0;
      timeout.tv_usec=200000;

      FD_ZERO(&rfds); FD_SET(fdi, &rfds); continue; 
    }
  
}

  /* Ripulitura all'uscita */
  close(fdi); close(fdo);
  werase(pw); delwin(pw);
  clear(); refresh(); endwin();
  return 0;
}


void set_az(char *os){
  float az;
  curs_set(1); timeout(-1); echo();
  mvwprintw(pw, 4,4, "AZ=                                  ");
  wrefresh(pw); mvwscanw(pw, 4,8, "%f", &az);
  if(az<0. || az>360.) {
    mvwprintw(pw, 4,4, "                                   ");
    wattron(pw, COLOR_PAIR(2) | A_BOLD);
    mvwprintw(pw, 4,4, "Value out of range: 0-360 allowed!"); 
    wattroff(pw, A_BOLD); wattron(pw, COLOR_PAIR(1));
    wrefresh(pw); sleep(1);
  }
  else 
    sprintf(os, "az %.1f\n", az);

  mvwprintw(pw, 4,4, "                                   "); wrefresh(pw);
  curs_set(0); timeout(0); noecho();
}

void set_el(char *os){
  float el;
  curs_set(1); timeout(-1); echo();
  mvwprintw(pw, 4,4, "EL=                                 ");
  wrefresh(pw); mvwscanw(pw, 4,8, "%f", &el);
  if(el<0. || el>180.) {
    mvwprintw(pw, 4,4, "                                   ");
    wattron(pw, COLOR_PAIR(2) | A_BOLD);
    mvwprintw(pw, 4,4, "Value out of range: 0-180 allowed!"); 
    wattroff(pw, A_BOLD); wattron(pw, COLOR_PAIR(1));
    wrefresh(pw); sleep(1);
  }
  else 
    sprintf(os, "el %.1f\n", el);

  mvwprintw(pw, 4,4, "                                   "); wrefresh(pw);
  curs_set(0); timeout(0); noecho();

}

void set_hist(char *os){
  float hist;
  curs_set(1); timeout(-1); echo();
  mvwprintw(pw, 4,4, "HIST=                              ");
  wrefresh(pw); mvwscanw(pw, 4,10, "%f", &hist);
  if(hist<0. || hist>25.5) {
    mvwprintw(pw, 4,4, "                                  ");
    wattron(pw, COLOR_PAIR(2) | A_BOLD);
    mvwprintw(pw, 4,4, "Value out of range: 0-25 allowed!"); 
    wattroff(pw, A_BOLD); wattron(pw, COLOR_PAIR(1));
    wrefresh(pw); sleep(1);
  }
  else 
    sprintf(os, "hist %.1f\n", hist);

  mvwprintw(pw, 4,4, "                                  "); wrefresh(pw);
  curs_set(0); timeout(0); noecho();

}

void set_step(char *os){
  float step;
  curs_set(1); timeout(-1); echo();
  mvwprintw(pw, 4,4, "STEP=                                ");
  wrefresh(pw); mvwscanw(pw, 4,10, "%f", &step);
  if(step<0. || step>25.5) {
    mvwprintw(pw, 4,4, "                                    ");
    wattron(pw, COLOR_PAIR(2) | A_BOLD);
    mvwprintw(pw, 4,4, "Value out of range: 0-25.5 allowed!"); 
    wattroff(pw, A_BOLD); wattron(pw, COLOR_PAIR(1));
    wrefresh(pw); sleep(1);
  }
  else 
    sprintf(os, "step %.1f\n", step);

  mvwprintw(pw, 4,4, "                                     "); wrefresh(pw);
  curs_set(0); timeout(0); noecho();

}

void set_offaz(char *os){
  float offaz;
  curs_set(1); timeout(-1); echo();
  mvwprintw(pw, 4,4, "offsetAZ=                            ");
  wrefresh(pw); mvwscanw(pw, 4,14, "%f", &offaz);
  if(offaz<0. || offaz>25.5) {
    mvwprintw(pw, 4,4, "                                    ");
    wattron(pw, COLOR_PAIR(2) | A_BOLD);
    mvwprintw(pw, 4,4, "Value out of range: 0-25 allowed!"); 
    wattroff(pw, A_BOLD); wattron(pw, COLOR_PAIR(1));
    wrefresh(pw); sleep(1);
  }
  else 
    sprintf(os, "offaz %.1f\n", offaz);

  mvwprintw(pw, 4,4, "                                     "); wrefresh(pw);
  curs_set(0); timeout(0); noecho();

}

void set_offel(char *os){
  float offel;
  curs_set(1); timeout(-1); echo();
  mvwprintw(pw, 4,4, "offsetEL=                            ");
  wrefresh(pw); mvwscanw(pw, 4,14, "%f", &offel);
  if(offel<0. || offel>25.5) {
    mvwprintw(pw, 4,4, "                                    ");
    wattron(pw, COLOR_PAIR(2) | A_BOLD);
    mvwprintw(pw, 4,4, "Value out of range: 0-25.5 allowed!"); 
    wattroff(pw, A_BOLD); wattron(pw, COLOR_PAIR(1));
    wrefresh(pw); sleep(1);
  }
  else 
    sprintf(os, "offel %.1f\n", offel);

  mvwprintw(pw, 4,4, "                                    "); wrefresh(pw);
  curs_set(0); timeout(0); noecho();

}

int open_fifo(int *fdi, int *fdo, int create, char *path){

int mode;
char HomeDir[40], cfgdir[40], gf[80];

 if(path[0]=='\0') {
   mode=F_OK; strcpy(HomeDir, getenv("HOME"));
   if(HomeDir==NULL) {
     printf("Unable to find HOME dir ?!\n"); return -1;
   }
   strcat(HomeDir, "/");
   strcpy(cfgdir, HomeDir); strcat(cfgdir, ROTORDIR);
 }
 else {
   strcpy(cfgdir, path);
   if(cfgdir[strlen(cfgdir)-1]!='/') strcat(cfgdir, "/");
 }

 if(access(cfgdir, mode)==-1) {
   printf("Unable to find a \"%s\" dir\n", cfgdir);
   return -1;
 }   
 
 mode=F_OK; strcpy(gf, cfgdir); strcat(gf, FIFOINNAME);
 if(access(gf, mode)==-1){
   if(create==0) {
     printf("Unable to find \"%s\"\n", gf); return -1;
   }
   else {
     if(mkfifo(gf, 0600)==-1) {
       fprintf(stderr, "Unable to create input fifo \"%s\"\n", gf);
       return -1;
     }
   }
 }

 if((*fdi=open(gf, O_RDWR | O_NDELAY))==-1) {
   fprintf(stderr, "Unable to open input fifo \"%s\"\n", gf);
   return -1;
 }

 mode=F_OK; strcpy(gf, cfgdir); strcat(gf, FIFOOUTNAME);
 if(access(gf, mode)==-1){
   if(create==0) {
     printf("Unable to find \"%s\"\n", gf); return -1;
   }
   else {
     if(mkfifo(gf, 0600)==-1) {
       fprintf(stderr, "Unable to create output fifo \"%s\"\n", gf);
       return -1;
     }
   }
 }

 if((*fdo=open(gf, O_RDWR | O_NDELAY))==-1) {
   fprintf(stderr, "Unable to open output fifo \"%s\"\n", gf);
   return -1;
 }
 
 
 
 return 0;
}

